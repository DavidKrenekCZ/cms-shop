<?php

return [
    'shop.orders' => [
        'index' => 'shop::orders.list resource',
    ],
    'shop.settings' => [
        'index' => 'shop::settings.edit resource',
    ],
];
