<?php

namespace Modules\Shop\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterShopSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('shop::shop.title'), function (Item $item) {
                $item->icon('fa fa-shopping-cart');
                $item->weight(10);
                $item->authorize(
                    $this->auth->hasAccess('shop.orders.index') || 
                    $this->auth->hasAccess('shop.settings.index') || 
                    $this->auth->hasAccess('dynamicpages.templates.index') ||
                    $this->auth->hasAccess('dynamicpages.records.index') ||
                    $this->auth->hasAccess('dynamicpages.categories.index')
                );

                $item->item(trans('shop::products.title'), function (Item $item) {
                    $item->icon('fa fa-shopping-bag');
                    $item->weight(0);
                    $item->append('admin.dynamicpages.record.create');
                    $item->route('admin.dynamicpages.record.index', [
                        "template" => \Modules\Shop\Entities\Product::getTemplateId()
                    ]);
                    $item->authorize(
                        $this->auth->hasAccess('dynamicpages.records.index')
                    );
                });
                $item->item(trans('shop::orders.title'), function (Item $item) {
                    $item->icon('fa fa-usd');
                    $item->weight(0);
                    $item->route('admin.shop.order.index');
                    $item->authorize(
                        $this->auth->hasAccess('shop.orders.index')
                    );
                });
                $item->item(trans('shop::settings.title'), function (Item $item) {
                    $item->icon('fa fa-cog');
                    $item->weight(0);
                    $item->route('admin.shop.settings.index');
                    $item->authorize(
                        $this->auth->hasAccess('shop.settings.index')
                    );
                });
            });
        });

        return $menu;
    }
}
