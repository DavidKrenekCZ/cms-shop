<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopShippingMethodTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop__shippingmethod_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->string("name");
            $table->string("note")->default("")->nullable();

            $table->integer('sm_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['sm_id', 'locale']);
            $table->foreign('sm_id')->references('id')->on('shop__shippingmethods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop__shippingmethod_translations', function (Blueprint $table) {
            $table->dropForeign(['sm_id']);
        });
        Schema::dropIfExists('shop__shippingmethod_translations');
    }
}
