<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCartCountryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop__cartcountry_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->string("short", 20);
            $table->string("name");

            $table->integer('cc_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['cc_id', 'locale']);
            $table->foreign('cc_id')->references('id')->on('shop__cartcountries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop__cartcountry_translations', function (Blueprint $table) {
            $table->dropForeign(['cc_id']);
        });
        Schema::dropIfExists('shop__cartcountry_translations');
    }
}
