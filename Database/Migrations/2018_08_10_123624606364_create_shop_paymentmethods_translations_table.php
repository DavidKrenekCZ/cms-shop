<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopPaymentMethodsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop__paymentmethods_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string("name");
            $table->string("note")->default("")->nullable();

            $table->integer('pm_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['pm_id', 'locale']);
            $table->foreign('pm_id')->references('id')->on('shop__paymentmethods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop__paymentmethods_translations', function (Blueprint $table) {
            $table->dropForeign(['pm_id']);
        });
        Schema::dropIfExists('shop__paymentmethods_translations');
    }
}
