<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeTablesSoftDeletable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop__orders', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('shop__cartcountries', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('shop__paymentmethods', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('shop__shippingmethods', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop__orders', function (Blueprint $table) {
            $table->dropColumn("deleted_at");
        });

        Schema::table('shop__cartcountries', function (Blueprint $table) {
            $table->dropColumn("deleted_at");
        });

        Schema::table('shop__paymentmethods', function (Blueprint $table) {
            $table->dropColumn("deleted_at");
        });

        Schema::table('shop__shippingmethods', function (Blueprint $table) {
            $table->dropColumn("deleted_at");
        });
    }
}
