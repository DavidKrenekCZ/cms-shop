<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;

class ShippingMethodTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ["name", "note"];
    protected $table = 'shop__shippingmethod_translations';
}
