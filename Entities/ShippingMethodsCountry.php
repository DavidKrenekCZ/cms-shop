<?php

namespace Modules\Shop\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ShippingMethodsCountry extends Model
{
	public $timestamps = false;
	
    protected $table = 'shop__shippingmethodscountries';
    protected $fillable = [ 'shipping_method_id', 'shipping_country_id' ];

    /* --- RELATIONS --- */
    public function country() {
        return $this->belongsTo("\Modules\Shop\Entities\CartCountry", 'id', 'shipping_country_id');
    }

    public function shipping() {
        return $this->belongsTo("\Modules\Shop\Entities\ShippingMethod");
    }
}
