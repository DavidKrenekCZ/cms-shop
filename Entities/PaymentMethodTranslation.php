<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;

class PaymentMethodTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ["name", "note"];
    protected $table = 'shop__paymentmethods_translations';
}
