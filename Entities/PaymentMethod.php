<?php

namespace Modules\Shop\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
	use SoftDeletes;
    use Translatable;

    protected $table = 'shop__paymentmethods';
    protected $translationForeignKey = "pm_id";
    public $translatedAttributes = ["name", "note"];
    protected $fillable = ["price"];

    /* --- RELATIONS --- */
    public function shippingRelations() {
        return $this->hasMany("\Modules\Shop\Entities\ShippingMethodsCountry", "shipping_payment_methods_id");
    }     
}
