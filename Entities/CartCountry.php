<?php

namespace Modules\Shop\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class CartCountry extends Model
{
	use SoftDeletes;
    use Translatable;

    protected $table = 'shop__cartcountries';
    protected $translationForeignKey = "cc_id";
    public $translatedAttributes = [ "short", "name" ];
    protected $fillable = [];

    /* --- RELATIONS --- */
    public function shippingRelations() {
        return $this->hasMany("\Modules\Shop\Entities\ShippingMethodsCountry", 'shipping_country_id');
    } 

    /* --- ATTRIBUTES --- */
    public $appends = ["shippingMethodsIds"];

    public function getShippingMethodsIdsAttribute() {
        $this->load("shippingRelations");
        return $this->shippingRelations->pluck('shipping_method_id')->toArray();
    }
}
