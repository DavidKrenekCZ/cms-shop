<?php

namespace Modules\Shop\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Product;

use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
	use SoftDeletes;

    protected $table = 'shop__orders';
    protected $fillable = [ "user_data", "products_data", "delivery_data" ];

    /* --- ATTRIBUTES --- */
    public $append = ["user", "userTranslated", "delivery", "products", "price", "productsPrice"];

    public function getUserAttribute() {
        return json_decode($this->user_data);
    }

    // User data with translation
    public function getUserTranslatedAttribute() {
    	$return = [];

    	$dict = trans("shop::orders-user-data");

    	$customDict = config("asgard.shop.core.customer data dict", false);
    	if ($customDict)
    		$customDict = trans($customDict);

    	foreach ($this->user as $attribute => $value) {
    		// TODO: implement setting translation table in configuration
    		/*
			
    		*/
    		if (isset($customDict[$attribute]))
    			$name = $customDict[$attribute];
    		elseif (isset($dict[$attribute]))
    			$name = $dict[$attribute];
    		else
    			$name = str_replace(["-", "_", "."], " ", ucfirst($attribute));

    		$return[] = (object)[
    			"attribute" => $attribute,
    			"name" => $name,
    			"value" => $value,
    			"isEmpty" => trim($value) == "" or trim($value) == "-"
    		];
    	}

    	return (object)$return;
    }

    public function getDeliveryAttribute() {
    	$json = json_decode($this->delivery_data);

    	$json->price = $json->shipping->price + $json->payment->price;

        return $json;
    }

    public function getProductsAttribute() {
        $products = json_decode($this->products_data);
        $return = [];

        foreach ($products as $index => $product) {
            $p = Product::find($product->id);

            $pr = clone $product;

            // Product was deleted - try to recover it using Soft Delete
            if (!$p) {
            	$p = Product::withTrashed()->find($product->id);
            	$pr->deleted = true;
            } else
            	$pr->deleted = false;

            $pr->model = $p;
            $return[] = $pr;
        }

        return $return;
    }

    public function getProductsPriceAttribute() {
        $price = 0;

        foreach ($this->products as $product)
            $price = $price+($product->price*$product->quantity);

        return $price;
    }

    public function getPriceAttribute() {
        return $this->productsPrice + $this->delivery->price;
    }
}
