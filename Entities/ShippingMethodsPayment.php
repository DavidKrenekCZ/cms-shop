<?php

namespace Modules\Shop\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ShippingMethodsPayment extends Model
{
	public $timestamps = false;

    protected $table = 'shop__shippingmethodspayments';
    protected $fillable = [ 'shipping_method_id', 'shipping_payment_method_id' ];

    /* --- RELATIONS --- */
    public function payment() {
        return $this->belongsTo("\Modules\Shop\Entities\PaymentMethod", "id", "shipping_payment_methods_id");
    }

    public function shipping() {
        return $this->belongsTo("\Modules\Shop\Entities\ShippingMethod");
    }    
}
