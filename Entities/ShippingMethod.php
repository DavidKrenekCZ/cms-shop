<?php

namespace Modules\Shop\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use \Modules\Shop\Entities\CartCountry;
use \Modules\Shop\Entities\PaymentMethod;

use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingMethod extends Model
{
    use SoftDeletes;
    use Translatable;

    protected $table = 'shop__shippingmethods';
    protected $translationForeignKey = "sm_id";
    public $translatedAttributes = ["name", "note"];
    protected $fillable = ["price"];

    /* --- RELATIONS --- */
    public function countriesRelations() {
        return $this->hasMany("\Modules\Shop\Entities\ShippingMethodsCountry");
    }

    public function paymentsRelations() {
        return $this->hasMany("\Modules\Shop\Entities\ShippingMethodsPayment");
    }


    /* --- ATTRIBUTES --- */
    public $appends = [ "countriesIds", "countries", "paymentsIds", "payments" ];

    public function getCountriesIdsAttribute() {
    	$this->load("countriesRelations");
        return $this->paymentsRelations->pluck('shipping_country_id')->toArray();
    }

    public function getCountriesAttribute() {
        $ids = $this->countriesIds;
        return CartCountry::whereIn($ids)->get();
    }

    public function getPaymentsIdsAttribute() {
    	$this->load("paymentsRelations");
        return $this->paymentsRelations->pluck('shipping_payment_method_id')->toArray();
    }

    public function getPaymentsAttribute() {
        $ids = $this->paymentsIds;
        return PaymentMethod::whereIn($ids)->get();
    }


    /* --- METHODS --- */
    public function hasCountry($countryId) {
    	return in_array($countryId, $this->countriesIds);
    }

    public function hasPayment($paymentId) {
    	return in_array($paymentId, $this->paymentsIds);
    }
}
