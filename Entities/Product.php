<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\DynamicPages\Entities\Template;
use Modules\DynamicPages\Entities\TemplateField;
use Modules\DynamicPages\Entities\TemplateTranslation;
use Modules\DynamicPages\Entities\Record;

class Product extends Record {
	private const TEMPLATE_DEFAULTS = [
		"template" => [
			"settings_files" 	=> 1,
			"settings_images" 	=> 1,
			"settings_url" 		=> 1,
			"settings_meta" 	=> 1,
			"category" 			=> 1,
			"main_image" 		=> 1,
			"cs" => [
				"name" => "Produkt obchodu"
			],
			"en" => [
				"name" => "Shop product"
			]
		],
		"fields" => [
			[
				"type" => "input",
				"cs" => [ "name" => "Kód" ],
				"en" => [ "name" => "Code" ]
			], [
				"type" => "input",
				"cs" => [ "name" => "Cena" ],
				"en" => [ "name" => "Price" ]
			], [
				"type" => "input",
				"cs" => [ "name" => "SEO - titulek" ],
				"en" => [ "name" => "SEO - title" ]
			], [
				"type" => "input",
				"cs" => [ "name" => "Poznámka" ],
				"en" => [ "name" => "Note" ]
			], [
				"type" => "wysiwyg",
				"cs" => [ "name" => "Popis" ],
				"en" => [ "name" => "Description" ]
			]
		]
	];

	/**
	 * Create default product template if doesn't exist already
	 */
	public static function createTemplate() {
		if (self::templateAlreadyExists())
			return false;

		$defaults = (object)self::TEMPLATE_DEFAULTS;

		// Is English supported locale or not?
		$english = isset(\LaravelLocalization::getSupportedLocales()["en"]);

		$template = Template::create($defaults->template);

		// English is not set as supported locale - force translate template
		if (!$english) {
			$template->translateOrNew("en")->name = $defaults->template["en"]["name"];
			$template->save();
		}

        foreach ($defaults->fields as $index => $value) {
            $value["template_id"] = $template->id;
            $value["position"] = $index+1;

            $field = TemplateField::create($value);

            // English is not set as supported locale - force translate field
            if (!$english) {
            	$field->translateOrNew("en")->name = $value["en"]["name"];
            	$field->save();
            }
        }
	}

	/**
	 * Check wheather product template was already created or not
	 * @return boolean
	 */
	private static function templateAlreadyExists() {
		return self::getTemplateId() != 0;
	}

	/**
	 * Get ID of product template
	 * @return integer
	 */
	public static function getTemplateId() {
		$translation = \Modules\DynamicPages\Entities\TemplateTranslation::where([
				[ "name", self::TEMPLATE_DEFAULTS["template"]["en"]["name"] ],
				[ "locale", "en" ]
			])->orWhere([
				[ "name", self::TEMPLATE_DEFAULTS["template"]["cs"]["name"] ],
				[ "locale", "cs" ]
			])->first();

		if ($translation)
			return $translation->template_id;
		return 0;
	}

	/**
	 * Overwrite default all function to only get products
	 *
	 * @param bool 	$visibleOnly = true
	 * @return Collection
	 */
	public static function all($visibleOnly = true) {
		$collection = self::where("template_id", self::getTemplateId());

		if ($visibleOnly)
			$collection->where([["visible", 1], ["published_since", "<", time()]])->orderBy('published_since', 'desc')->orderBy('id', 'desc');

		return $collection->get();
	}

	/**
	 * Overwrite default find function to only get product
	 *
	 * @return Product
	 */
	public static function find($id) {
		return self::where([
			[ "template_id", self::getTemplateId() ],
			[ "id", $id ]
		])->first();
	}

	/**
	 * Find product by URL slug
	 *
	 * @param string 	$slug 
	 * @param int 		$categoryId = null
	 * @return Product|null
	 */
	public static function findBySlug($slug, $categoryId = null) {
		// TODO - implement filter by category
		$record = \Modules\DynamicPages\Http\Controllers\DynamicPagesController::getItemFromURL($slug);

		if (!$record || get_class($record) != "Modules\DynamicPages\Entities\Record" || ($categoryId && !$record->isInCategory($categoryId)) || $record->template_id != self::getTemplateId())
			return null;

		return Product::find($record->id);
	}

	/**
	 * Find product by URL slug or die
	 *
	 * @param string 	$slug 
	 * @param int 		$categoryId = null
	 * @return Product|null
	 */
	public static function findBySlugOrFail($slug, $categoryId = null) {
		$product = self::findBySlug($slug, $categoryId);
		if (!$product)
			abort(404);
		return $product;
	}

	/**
	 * Get field of product
	 *
	 * @param string 	$key
	 * @param bool 		$allowAutoFormat = true
	 * @return mixed
	 */
	public function field($key, $allowAutoFormat = true) {
		$field = $this->getFieldByName($key, "en");
		if ($field) {
			$value = $field->value;
			if (!$allowAutoFormat)
				return $value;

			if ($key == "Price")
				$value = price($value);
			return $value;
		}
		return "";
	}
}
