<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;

class CartCountryTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [ "short", "name" ];
    protected $table = 'shop__cartcountry_translations';
}
