<?php

namespace Modules\Shop\Facades;

use Gloudemans\Shoppingcart\Facades\Cart as ShoppingCart;
use Modules\Shop\Entities\Product;

class Cart {
    /**
     * Insert item to cart
     *
     * @param int   $productId
     * @param int   $quantity = 1
     * @return bool
     */
    public static function insertItem($productId, $quantity = 1) {
        $product = Product::find($productId);

        if (!$product)
            return false;

        return (ShoppingCart::add([[
            "id" => $product->id, 
            "name" => $product->name, 
            "qty" => $quantity, 
            "price" => $product->field("Price", false), 
            "options" => [
                "code" => $product->field("Code", false),
                "image" => explode("?updated", $product->mainImage)[0],
            ]
        ]]));
    }

    /**
     * Update item quantity in cart
     *
     * @param int   $cartRowId
     * @param int   $quantity
     * @return bool
     */
    public static function updateItem($cartRowId, $quantity) {
        // Test if row ID is valid and exists
        if (!ShoppingCart::content()->has($cartRowId) || $quantity < 0)
            return false;

        $result = ShoppingCart::update($cartRowId, $quantity);

        // When quantity is 0, update method returns void
        if ($quantity > 0)
            return $result;
        return true;
    }

    /**
     * Remove item from cart
     *
     * @param int   $cartRowId
     * @return bool
     */
    public static function removeItem($cartRowId) {
        // Test if row ID is valid and exists
        if (!ShoppingCart::content()->has($cartRowId))
            return false;

        ShoppingCart::remove($cartRowId); // remove method returns void

        return true;
    }

    /**
     * Destroy whole content of cart
     *
     * @return bool
     */
    public static function destroyCart() {
        ShoppingCart::destroy(); // destroy method returns void

        return true;
    }



    /**
     * Return Cart content
     *
     * @return Collection
     */
    public static function getCartContent() {
        return ShoppingCart::content();
    }

	/**
     * Return formatted Cart subtotal
     *
     * @param   bool    $format = true
     * @return  stirng
     */
    public static function getCartSubtotal($format = true) {
        $amount = str_replace(",", "", ShoppingCart::subtotal());
        if (!$format)
            return $amount;
        return price($amount);
    }

    /**
     * Return number of unique items in cart
     *
     * @return int
     */
    public static function getCartCount() {
        return ShoppingCart::content()->count();
    }
}