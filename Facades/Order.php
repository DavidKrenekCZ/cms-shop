<?php

namespace Modules\Shop\Facades;

use Modules\Shop\Facades\Cart as CartFacade;
use Modules\Shop\Entities\Product;
use Modules\Shop\Entities\CartCountry;
use Modules\Shop\Entities\ShippingMethod;
use Modules\Shop\Entities\PaymentMethod;
use Modules\Shop\Entities\Order as OrderEntity;

class Order {
    /**
     * Store user data to be submitted with order later
     *
     * @param array             $userData
     * @param string|array      $ignoreKeys = []    (e. g. "_token")
     * @return bool
     */
    public static function storeUserData($userData, $ignoreKeys = []) {
        // Remove ignored keys (e. g. token)
        if (is_array($ignoreKeys)) {
            foreach ($ignoreKeys as $key)
                unset($userData[$key]);
        } else
            unset($userData[$ignoreKeys]);

        return self::storeData("orderUserData", $userData);
    }

    /**
     * Retrieve user data that was previously stored
     *
     * @return array|bool
     */
    public static function getStoredUserData() {
        return self::retrieveData("orderUserData");
    }

    /**
     * Delete user data
     *
     * @return bool
     */
    public static function deleteStoredUserData() {
        return self::deleteData("orderUserData");
    }

    /**
     * Store delivery data to be submitted with order later
     *
     * @param array             $deliveryData       (format: [ country: CartCountryID, shipping: ShippingMethodID, payment: PaymentMethodId ])
     * @param string|array      $ignoreKeys = []    (e. g. "_token")
     * @return bool
     */
    public static function storeDeliveryData($deliveryData, $ignoreKeys = []) {
        // Remove ignored keys (e. g. token)
        if (is_array($ignoreKeys)) {
            foreach ($ignoreKeys as $key)
                unset($deliveryData[$key]);
        } else
            unset($deliveryData[$ignoreKeys]);

        // Validate the data
        if (!self::validateDeliveryData($deliveryData))
            return false;

        $deliveryData["countryModel"] = CartCountry::find($deliveryData["country"]);
        $deliveryData["paymentModel"] = PaymentMethod::find($deliveryData["payment"]);
        $deliveryData["shippingModel"] = ShippingMethod::find($deliveryData["shipping"]);

        return self::storeData("orderDeliveryData", $deliveryData);
    }

    /**
     * Retrieve delivery data that was previously stored
     *
     * @return array|bool
     */
    public static function getStoredDeliveryData() {
        return self::retrieveData("orderDeliveryData");
    }

    /**
     * Delete delivery data
     *
     * @return bool
     */
    public static function deleteStoredDeliveryData() {
        return self::deleteData("orderDeliveryData");
    }

    /**
     * Submit order
     *
     * @param array     $userData = null        (if not set, data is retrieved from the stored data)
     * @param array     $deliveryData = null    (if not set, data is retrieved from the stored data)
     * @param array     $productsData = null    (if not set, data is retrieved from cart) (format: [ [id: 1, quantity: 2], ... ])
     */
    public static function submitOrder($userData = null, $deliveryData = null, $productsData = null) {
        $orderData = [];

        // Parse user data
        if (!$userData) {
            $userData = self::getStoredUserData();

            if (!$userData)
                return false;
        }
        $orderData["user_data"] = json_encode($userData);

        // Parse delivery data
        if (!$deliveryData)
            $deliveryData = self::getStoredDeliveryData();

        $deliveryData = self::validateDeliveryData($deliveryData);
        if (!$deliveryData)
            return false;

        unset($deliveryData["countryModel"], $deliveryData["paymentModel"], $deliveryData["shippingModel"]);

        $orderData["delivery_data"] = [];
        foreach ($deliveryData as $index => $model) {
            $data = [
                "id" => $model->id,
                "name" => $model->name,
            ];

            if (isset($model->price))
                $data["price"] = $model->price;

            $orderData["delivery_data"][$index] = $data;
        }
        $orderData["delivery_data"] = json_encode($orderData["delivery_data"]);


        // Parse products data
        $orderData["products_data"] = [];
        if ($productsData) {
            // Get data from passed products array
            foreach ($productsData as $product) {
                $data = [
                    "id" => $product["id"],
                    "quantity" => $product["quantity"]
                ];

                $productModel = Product::findOrFail($product["id"]);
                $data["name"] = $productModel->name;
                $data["price"] = $productModel->field("Price");
                
                $orderData["products_data"][] = $data;
            }
        } else {
            // Get data from cart
            $cart = CartFacade::getCartContent();
            foreach ($cart as $item)
                $orderData["products_data"][] = [
                    "id"        => $item->id,
                    "quantity"  => $item->qty,
                    "price"     => $item->price,
                    "name"      => $item->name
                ];
        }

        $orderData["products_data"] = json_encode($orderData["products_data"]);

        $order = OrderEntity::create($orderData);
        if ($order) {
            CartFacade::destroyCart();
            self::deleteStoredDeliveryData();
            self::deleteStoredUserData();
            return $order;
        }
        return false;
    }










    /**
     * Store data
     *
     * @param string    $key
     * @param data      array
     * @return bool
     */
    private static function storeData($key, $data) {
        session([ $key => $data ]);
        return session()->has($key);
    }

    /**
     * Retrieve data
     *
     * @param string    $key
     * @return bool
     */
    private static function retrieveData($key) {
        if (!session()->has($key))
            return false;

        return session($key);
    }

    /**
     * Delete data
     * 
     * @param string    $key
     * @return bool
     */
    private static function deleteData($key) {
        session()->forget($key);
        return !session()->has($key);
    }

    /**
     * Validate delivery data
     *
     * @param array     $data
     * @return bool|array
     */
    private static function validateDeliveryData($data) {
        // Required keys missing
        if (!is_array($data) || !isset($data["country"]) || !isset($data["shipping"]) || !isset($data["payment"]))
            return false;

        $result = [
            "country" => CartCountry::find($data["country"]),
            "shipping" => ShippingMethod::find($data["shipping"]),
            "payment" => PaymentMethod::find($data["payment"])
        ];

        // Country, Shipping method or Payment method not found
        if (!$result["country"] || !$result["shipping"] || !$result["payment"])
            return false;

        return $result;
    }    
}