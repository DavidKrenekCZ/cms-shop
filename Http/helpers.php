<?php

if (!function_exists("price")) {
	// Format price
	function price($value) {
		$currency = config("asgard.shop.core.currency.string", " Kč");
		$currencyBeforeNumber = config("asgard.shop.core.currency.before number", false);

		$decimals = config("asgard.shop.core.currency.decimals", "auto");
		$decimalSeparator = config("asgard.shop.core.currency.decimalSeparator", ",");
		$thousandSeparator = config("asgard.shop.core.currency.thousandSeparator", " ");

		// Auto detect decimal number
		if ($decimals == "auto") {
			if (floor($value*100/100) == $value) 	// number has no decimals
				$decimals = 0;
			else 									// number has decimals
				$decimals = 2;
		}

		return ($currencyBeforeNumber ? $currency : "").number_format($value, $decimals, $decimalSeparator, $thousandSeparator).(!$currencyBeforeNumber ? $currency : "");
	}
}