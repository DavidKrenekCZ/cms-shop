<?php

namespace Modules\Shop\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateCartCountryRequest extends BaseFormRequest
{
    protected $translationsAttributesKey = 'shop::cartcountries.form';

    public function rules()
    {
        return [];
    }

    public function translationRules()
    {
        return [
            "name" => "required",
            "short" => "required"
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }

    protected function getRedirectUrl() {
        $url = $this->redirector->getUrlGenerator();
        return $url->previous()."#form-countries";
    }
}
