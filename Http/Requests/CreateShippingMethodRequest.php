<?php

namespace Modules\Shop\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateShippingMethodRequest extends BaseFormRequest
{
    protected $translationsAttributesKey = 'shop::shippingmethods.form';

    public function rules()
    {
        return [
            "price" => "required|numeric"
        ];
    }

    public function translationRules()
    {
        return [
            "name" => "required",
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }

    protected function getRedirectUrl() {
        $url = $this->redirector->getUrlGenerator();
        return $url->previous()."#form-shipping";
    }    
}
