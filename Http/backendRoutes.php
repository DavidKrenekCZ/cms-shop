<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/shop'], function (Router $router) {
    $router->bind('order', function ($id) {
        return app('Modules\Shop\Repositories\OrderRepository')->find($id);
    });
    $router->get('orders', [
        'as' => 'admin.shop.order.index',
        'uses' => 'OrderController@index',
        'middleware' => 'can:shop.orders.index'
    ]);
    $router->get('orders/{order}', [
        'as' => 'admin.shop.order.detail',
        'uses' => 'OrderController@detail',
        'middleware' => 'can:shop.orders.index'
    ]);
    $router->post('orders/change-done-state', [
        'as' => 'admin.shop.order.change done state',
        'uses' => 'OrderController@changeDoneState',
        'middleware' => 'can:shop.orders.index'
    ]);
    $router->delete('orders/{order}', [
        'as' => 'admin.shop.order.destroy',
        'uses' => 'OrderController@destroy',
        'middleware' => 'can:shop.orders.index'
    ]);






    $router->get('settings', [
        'as' => 'admin.shop.settings.index',
        'uses' => 'SettingsController@index',
        'middleware' => 'can:shop.settings.index'
    ]);



    // --- Cart countries ---

    // Create
    $router->post('cartcountries/create', [
        'as' => 'admin.shop.cartcountry.store',
        'uses' => 'CartCountryController@store',
        'middleware' => 'can:shop.settings.index'
    ]);

    // Edit
    $router->post('cartcountries/edit/{id}', [
        'as' => 'admin.shop.cartcountry.edit',
        'uses' => 'CartCountryController@edit',
        'middleware' => 'can:shop.settings.index'
    ]);

    // Delete
    $router->get('cartcountries/delete/{id}', [
        'as' => 'admin.shop.cartcountry.destroy',
        'uses' => 'CartCountryController@destroy',
        'middleware' => 'can:shop.settings.index'
    ]);



    // --- Payment methods ---
    
    // Create
    $router->post('paymentmethods/create', [
        'as' => 'admin.shop.paymentmethods.store',
        'uses' => 'PaymentMethodsController@store',
        'middleware' => 'can:shop.settings.index'
    ]);
    
    // Edit
    $router->post('paymentmethods/edit/{id}', [
        'as' => 'admin.shop.paymentmethods.edit',
        'uses' => 'PaymentMethodsController@edit',
        'middleware' => 'can:shop.settings.index'
    ]);

    // Delete
    $router->get('paymentmethods/delete/{id}', [
        'as' => 'admin.shop.paymentmethods.destroy',
        'uses' => 'PaymentMethodsController@destroy',
        'middleware' => 'can:shop.settings.index'
    ]);



    // --- Shipping methods ---

    // Create
    $router->post('shippingmethods/create', [
        'as' => 'admin.shop.shippingmethods.store',
        'uses' => 'ShippingMethodsController@store',
        'middleware' => 'can:shop.settings.index'
    ]);

    // Edit
    $router->post('shippingmethods/edit/{id}', [
        'as' => 'admin.shop.shippingmethods.edit',
        'uses' => 'ShippingMethodsController@edit',
        'middleware' => 'can:shop.settings.index'
    ]);

    // Delete
    $router->get('shippingmethods/delete/{id}', [
        'as' => 'admin.shop.shippingmethods.destroy',
        'uses' => 'ShippingMethodsController@destroy',
        'middleware' => 'can:shop.settings.index'
    ]);

    // Edit payments relations
    $router->post('shippingmethods/edit-payments', [
        'as' => 'admin.shop.shippingmethods.edit.payments',
        'uses' => 'ShippingMethodsController@editPayments',
        'middleware' => 'can:shop.settings.index'
    ]);

    // Edit countries relations
    $router->post('shippingmethods/edit-countries', [
        'as' => 'admin.shop.shippingmethods.edit.countries',
        'uses' => 'ShippingMethodsController@editCountries',
        'middleware' => 'can:shop.settings.index'
    ]);










});
