<?php

$router->group([
	'prefix' 		=>'/shop-api',
	'namespace' 	=> 'Api',
	'middleware' 	=> 'Modules\Shop\Http\Middleware\ApiMiddleware'
], function ($router) {

	$router->group([
		"prefix" => "cart"
	], function($router) {

		// Insert item to cart
		$router->post("insert", [
			"uses"	=> "CartController@insertItem",
			"as"	=> "api.shop.cart.insert"
		]);

		// Update item in cart
		$router->post("update", [
			"uses"	=> "CartController@updateItem",
			"as"	=> "api.shop.cart.update"
		]);

		// Remove item from cart
		$router->post("remove", [
			"uses"	=> "CartController@removeItem",
			"as"	=> "api.shop.cart.remove"
		]);
	
		// Destroy cart content
		$router->post("destroy", [
			"uses"	=> "CartController@destroyCart",
			"as"	=> "api.shop.cart.destroy"
		]);
	});

	$router->group([
		"prefix" => "order"
	], function($router) {

		// Store user data to be used later
		$router->post("store-user-data", [
			"uses" 	=> "OrderController@storeUserData",
			"as"	=> "api.shop.order.store user data"
		]);

		// Store delivery data to be used later
		$router->post("store-delivery-data", [
			"uses" 	=> "OrderController@storeDeliveryData",
			"as"	=> "api.shop.order.store delivery data"
		]);

		// Store order data (user and delivery)
		$router->post("store-order-data", [
			"uses"	=> "OrderController@storeOrderData",
			"as"	=> "api.shop.order.store order data"
		]);

		// Submit order
		$router->post("submit-order", [
			"uses"	=> "OrderController@submitOrder",
			"as"	=> "api.shop.order.submit"
		]);

	});


	/*
	// Products routes examples

	$router->get("/products", [
		"uses" 	=> "\Modules\Shop\Http\Controllers\Frontend\ProductsController@overview",
		"as" 	=> "frontend.shop.products overview"
	]);

	$router->get("/products/{slug}", [
		"uses" 	=> "\Modules\Shop\Http\Controllers\Frontend\ProductsController@detail",
		"as" 	=> "frontend.shop.product detail"
	]);

	$router->get("/cart/{step?}", [
    	"uses"  => "\Modules\Shop\Http\Controllers\Frontend\CartController@step",
    	"as"    => "frontend.shop.cart"
	]);

	*/
	

});