<?php

namespace Modules\Shop\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Shop\Entities\Product;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ProductsController extends AdminBaseController
{

    /**
     * Get overview of products
     *
     * @param   int     $categoryId = null
     * @return  Response
     */
    public function overview($categoryId = null) {
        if ($categoryId)
            $products = Product::getByCategory($categoryId);
        else
            $products = Product::all();

        return view("shop::frontend.products", compact("products"));
    }

    /**
     * Get products detail
     *
     * @param string    $slug
     * @return Response
     */
    public function detail($slug) {
        $product = Product::findBySlugOrFail($slug);
        
        return view("shop::frontend.product", compact("product"));
    }
}
