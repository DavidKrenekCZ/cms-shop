<?php

namespace Modules\Shop\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Shop\Entities\Product;
use Modules\Shop\Facades\Cart;
use Modules\Shop\Facades\Order;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Shop\Entities\CartCountry;
use Modules\Shop\Entities\ShippingMethod;
use Modules\Shop\Entities\PaymentMethod;

class CartController extends AdminBaseController
{

    /**
     * Get cart step
     *
     * @param   int     $step = 1
     * @return  Response
     */
    public function step($step=1) {
        $content = Cart::getCartContent();

        // Cart empty - return to step one
        if ($step != 1 && count($content) < 1)
            return response()->redirectToRoute("frontend.shop.cart");

        return view("shop::frontend.cart-".$step, [
            "cartContent"   => $content,
            "userData"      => Order::getStoredUserData(),
            "deliveryData"  => Order::getStoredDeliveryData(),
            "countries" => CartCountry::all(),
            "shippings" => ShippingMethod::all(),
            "payments" => PaymentMethod::all()
        ]);
    }
}
