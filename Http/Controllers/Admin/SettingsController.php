<?php

namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Shop\Entities\CartCountry;
use Modules\Shop\Entities\ShippingMethod;
use Modules\Shop\Entities\PaymentMethod;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class SettingsController extends AdminBaseController
{
    /**
     * Show settings page
     *
     * @return Response
     */
    public function index()
    {
        return view('shop::admin.settings.index', [
            "countries" => CartCountry::all(),
            "shippings" => ShippingMethod::all(),
            "payments" => PaymentMethod::all()
        ]);
    }
}
