<?php

namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Shop\Entities\PaymentMethod;
use Modules\Shop\Http\Requests\CreatePaymentMethodRequest;
use Modules\Shop\Http\Requests\UpdatePaymentMethodRequest;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PaymentMethodsController extends AdminBaseController
{
    /**
     * Edit a field of this entity
     *
     * @param   int $id
     * @param   UpdatePaymentMethodRequest $request
     * @return  string
     */
    public function edit($id, UpdatePaymentMethodRequest $request) {
        $method = PaymentMethod::findOrFail($id);

        $value = trim($request->value);
        if ($value == "")
            return abort();

        $name = explode(":", trim($request->name));
        $update = [];
        
        if (count($name) == 1)
            $update = [
                $name[0] => $value
            ];
        else 
            $update = [
                $name[0] => [
                    $name[1] => $value
                ]
            ];

        return response()->json([
            "ok" => $method->update($update) ? true : false
        ]);
    }

    /**
     * Store a newly created entity
     *
     * @param  CreatePaymentMethodRequest $request
     * @return Response
     */
    public function store(CreatePaymentMethodRequest $request)
    {
        $posts = $request->all();
        unset($posts["_token"]);
        PaymentMethod::create($posts);

        return redirect()->route('admin.shop.settings.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('shop::paymentmethods.title_s')]));
    }

    /**
     * Remove record from DB
     *
     * @param   int $id
     * @return  Response
     */
    public function destroy($id)
    {
        PaymentMethod::findOrFail($id)->delete();

        return redirect()->route('admin.shop.settings.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('shop::paymentmethods.title_s')]));
    }
}
