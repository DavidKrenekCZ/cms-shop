<?php

namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Shop\Entities\Order;
use Modules\Shop\Http\Requests\CreateOrderRequest;
use Modules\Shop\Http\Requests\UpdateOrderRequest;
use Modules\Shop\Repositories\OrderRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class OrderController extends AdminBaseController
{
    /**
     * @var OrderRepository
     */
    private $order;

    public function __construct(OrderRepository $order)
    {
        parent::__construct();

        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $orders = $this->order->all();

        return view('shop::admin.orders.index', compact('orders'));
    }

    public function changeDoneState(Request $request) {
        $order = Order::findOrFail($request->id);

        $order->done = ($request->action == "done");

        if ($order->save()) {
            /*
            if ($request->action == "done") {
                Mail::send("mails.order-done", [
                    "orderId" => $order->id
                ], function ($m) use ($order) {
                    $m->from('info@spiritbeds.cz', 'Spirit Beds');
                    $m->to(json_decode($order->userData_string)->email)->subject('Objednávka úspěšně vyřízena!');
                });
            }
            */
            return response()->json(["ok" => true]);
        }
        return response()->json(["ok" => false]);
    }

    /**
     * Show the detail of an order
     *
     * @param  Order $order
     * @return Response
     */
    public function detail(Order $order)
    {
        return view('shop::admin.orders.detail', compact('order'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  Order $order
     * @return Response
     */
    public function destroy(Order $order)
    {
        $this->order->destroy($order);

        return redirect()->route('admin.shop.order.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('shop::orders.title_s')]));
    }
}
