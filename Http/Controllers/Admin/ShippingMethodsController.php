<?php

namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Shop\Entities\ShippingMethod;
use Modules\Shop\Http\Requests\CreateShippingMethodRequest;
use Modules\Shop\Http\Requests\UpdateShippingMethodRequest;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use \Modules\Shop\Entities\CartCountry;
use \Modules\Shop\Entities\PaymentMethod;
use \Modules\Shop\Entities\ShippingMethodsCountry;
use \Modules\Shop\Entities\ShippingMethodsPayment;

class ShippingMethodsController extends AdminBaseController
{

    /**
     * Store a newly created entity
     *
     * @param  CreateShippingMethodRequest $request
     * @return Response
     */
    public function store(CreateShippingMethodRequest $request)
    {
        $posts = $request->all();
        unset($posts["_token"]);
        ShippingMethod::create($posts);

        return redirect()->route('admin.shop.settings.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('shop::shippingmethods.title_s')]));
    } 

    /**
     * Edit a field of this entity
     *
     * @param   int $id
     * @param   UpdateShippingMethodRequest $request
     * @return  string
     */
    public function edit($id, UpdateShippingMethodRequest $request)
    {
        $method = ShippingMethod::findOrFail($id);

        $value = trim($request->value);
        if (!strpos($request->name, "note") && $value == "")
            return abort(404);

        $name = explode(":", trim($request->name));
        $update = [];
        
        if (count($name) == 1)
            $update = [
                $name[0] => $value
            ];
        else 
            $update = [
                $name[0] => [
                    $name[1] => $value
                ]
            ];

        return response()->json([
            "ok" => $method->update($update) ? true : false
        ]);
    }

    /**
     * Edit payments relations of shipping method
     *
     * @param   Request $request
     * @return  string
     */
    public function editPayments(Request $request) {
        return $this->editRelations($request, ShippingMethodsPayment::class, PaymentMethod::class, "shipping_payment_method_id");
    }

    /**
     * Edit countries relations of shipping method
     *
     * @param   Request $request
     * @return  string
     */
    public function editCountries(Request $request) {
        return $this->editRelations($request, ShippingMethodsCountry::class, CartCountry::class, "shipping_country_id");
    }

    /**
     * Edit countries/payments relations of shipping method
     *
     * @param   Request $request
     * @param   class   $relationClass
     * @param   class   $entityClass
     * @param   string  $relationColumn
     * @return  string
     */
    private function editRelations(Request $request, $relationClass, $entityClass, $relationColumn) {
        $shippingMethod = ShippingMethod::findOrFail($request->shippingId);

        $ids = $request->checkedIds;

        if (!is_array($ids) || count($ids) < 1)
            $ids = [];

        $savedIds = [];

        // Delete relations that are not selected
        $relationClass::where("shipping_method_id", $shippingMethod->id)->whereNotIn($relationColumn, $ids)->delete();

        foreach ($ids as $id) {
            if ($entityClass::find($id)) {
                $relationClass::firstOrCreate([
                    "shipping_method_id" => $shippingMethod->id,
                    $relationColumn => $id
                ]);
                $savedIds[] = $id;
            }
        }

        return response()->json(["ok" => true, "ids" => implode(",", $savedIds) ]);
    }

    /**
     * Remove record from DB
     *
     * @param   int $id
     * @return  Response
     */
    public function destroy($id)
    {
        ShippingMethod::findOrFail($id)->delete();

        return redirect()->route('admin.shop.settings.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('shop::shippingmethods.title_s')]));
    }
}
