<?php

namespace Modules\Shop\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Shop\Entities\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Shop\Facades\Order as OrderFacade;

class OrderController extends AdminBaseController
{
    /**
     * Store user data to be submitted with order later
     *
     * @param Request   $request
     * @return string
     */
    public function storeUserData(Request $request) {
        if (!isset($request->userData) && (!is_array($request->userData) || !is_object($request->userData)))
            return response()->json(["ok" => false]);

        $ignoreKeys = [];
        if (isset($request->ignoreKeys) && (is_array($request->ignoreKeys) || is_string($request->ignoreKeys)))
            $ignoreKeys = $request->ignoreKeys;

        if (OrderFacade::storeUserData($request->userData, $ignoreKeys))
            return response()->json(["ok" => true]);
        return response()->json(["ok" => false]);
    }

     /**
     * Store delivery data to be submitted with order later
     *
     * @param Request   $request
     * @return string
     */
    public function storeDeliveryData(Request $request) {
        if (!isset($request->deliveryData) && (!is_array($request->deliveryData) || !is_object($request->deliveryData)))
            return response()->json(["ok" => false]);

        $ignoreKeys = [];
        if (isset($request->ignoreKeys) && (is_array($request->ignoreKeys) || is_string($request->ignoreKeys)))
            $ignoreKeys = $request->ignoreKeys;
        
        if (OrderFacade::storeDeliveryData($request->deliveryData, $ignoreKeys))
            return response()->json(["ok" => true]);
        return response()->json(["ok" => false]);
    }

    /**
     * Store user and delivery data at once to be submitted with order later
     *
     * @param Request   $request
     * @return string
     */
    public function storeOrderData(Request $request) {
        $user = $this->storeUserData($request)->getData()->ok;
        $delivery = $this->storeDeliveryData($request)->getData()->ok;

        if ($user && $delivery)
            return response()->json(["ok" => true]);
        return response()->json(["ok" => false]);
    }

    /**
     * Submit order
     *
     * @param Request   $request
     * @return string
     */
    public function submitOrder(Request $request) {
        $userData = $productsData = $deliveryData = null;

        if (isset($request->userData) && (is_array($request->userData) || is_object($request->userData)))
            $userData = $request->userData;

        if (isset($request->deliveryData))
            $deliveryData = $request->deliveryData;

        if (isset($request->productsData) && (is_array($request->productsData) || is_object($request->productsData)))
            $productsData = $request->productsData;

        $order = OrderFacade::submitOrder($userData, $deliveryData, $productsData);

        if ($order)
            return response()->json([
                "ok" => true,
                "id" => $order->id
            ]);
        response()->json(["ok" => false]);
    } 
}
