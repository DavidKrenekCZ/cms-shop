<?php

namespace Modules\Shop\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Shop\Entities\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Shop\Facades\Cart as CartFacade;

class CartController extends AdminBaseController
{
    /**
     * Insert item to cart
     *
     * @param Request   $request
     * @return string
     */
    public function insertItem(Request $request) {
        // Product
        if (!isset($request->id))
            return response()->json(["ok" => false]);

        $id = (int)$request->id;

        // Quantity
        $quantity = isset($request->quantity) ? (int)$request->quantity : 1;

        if ($quantity <= 1)
            $quantity = 1; 

        if (CartFacade::insertItem($id, $quantity))
            return response()->json([
                "ok" => true,
                "cartItems" => CartFacade::getCartCount(),
                "cartPrice" => CartFacade::getCartSubtotal()
            ]);
        return response()->json(["ok" => false]);
    }

    /**
     * Update item quantity in cart
     *
     * @param Request   $request
     * @return string
     */
    public function updateItem(Request $request) {
        if (!isset($request->cartRowId) || !isset($request->quantity))
            return response()->json(["ok" => false]);

        if (CartFacade::updateItem($request->cartRowId, $request->quantity))
            return response()->json([
                "ok" => true,
                "cartItems" => CartFacade::getCartCount(),
                "cartPrice" => CartFacade::getCartSubtotal()
            ]);
        return response()->json(["ok" => false]);
    }

    /**
     * Remove item from cart
     *
     * @param Request   $request
     * @return string
     */
    public function removeItem(Request $request) {
        if (!isset($request->cartRowId))
            return response()->json(["ok" => false]);

        if (CartFacade::removeItem($request->cartRowId))
            return response()->json([
                "ok" => true,
                "cartItems" => CartFacade::getCartCount(),
                "cartPrice" => CartFacade::getCartSubtotal()
            ]);
        return response()->json(["ok" => false]);
    }

    /**
     * Destroy whole content of cart
     *
     * @return string
     */
    public function destroyCart() {
        CartFacade::destroyCart();

        return response()->json([
            "ok" => true,
            "cartItems" => CartFacade::getCartCount(),
            "cartPrice" => CartFacade::getCartSubtotal()
        ]);
    }
}
