<?php

namespace Modules\Shop\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ApiMiddleware
{
    /**
     * Handle an incoming API request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (
            $request->ajax() ||     // API request must be AJAX or in debug mode
            env("APP_DEBUG", false)
        )
            return $next($request);

        return response('Access denied', 403);
    }
}
