# Installation
- You need to add a few things to arrays in `config/app.php`
``` php
	'aliases' => [
		// ...
		'Image' => Intervention\Image\Facades\Image::class,
		// ...
	],

	'providers' => [
		// ...
		Intervention\Image\ImageServiceProvider::class,
		Spatie\Sitemap\SitemapServiceProvider::class,
		Gloudemans\Shoppingcart\ShoppingcartServiceProvider::class
		// ...
	]
```

# Settings
- All settings are taken from file `./config/asgard/shop/core.php` (see `./Modules/Shop/Config/core.example` for format)
- **Note:** This module requires `davidkrenekcz/dynamicpages` - it is recommended you read documentation at https://packagist.org/packages/davidkrenekcz/dynamicpages

### Translating customer data fields
- If you store user data with unusual keys (e. g. customer's card number) or just need to translate the keys to unusual language, the module won't be able to translate the keys and will leave them in English. 
- If you want to show them in your own format, just create a dictionary somewhere in your app and set the path to `customer data dict` settings attribute.
- The module will always try to translate the keys using your dictionary before using it's own.

# Products rendering
- Examples of product routes, controller mehotds and views are available in `\Modules\Shop\Http\frontendRoutes.php`, `\Modules\Shop\Http\Controllers\Frontend\ProductsController.php` and `\Modules\Shop\Resources\views\frontend\product(s).blade.php`

# Cart manipulation and order submitting - using module's JS controller
- You can manipulate content of cart and submit orders with ease through module's JS controller

## Include JS controller
- Add `@include("shop::frontend.partials.cart-ajax", [ "omit" => [ ] ])` to bottom of your page (after jQuery is loaded)
- You can customize which controller's methods you want to use through `omit` parameter
- `omit` values are
	* `cart-add`, `cart-update`, `cart-remove`, `cart-destroy`
	* `cart` (shortcut for all cart methods)
	* `order-customer-data`, `order-delivery-data`, `order-submit`
	* `order` (shortcut for all order methods)
- You can add them to the array in any combination you wish

## Cart content manipulation
#### Add product
- All you need to do is to add `data-add-to-cart-id={{ $product->id }}` attribute to your link and the controller will take care of the rest

#### Remove product
- When looping through cart content, add `data-remove-from-cart-id="{{ $item->rowId }}"` attribute to the delete link (where `$item` is cart row instance)

#### Update item count
- When looping through cart content, add `data-previous-value="{{ $item->qty }}"`  and `data-change-quantity-id="{{ $item->rowId }}"` attributes to your input (where `$item` is cart row instance)
- The controller will listen to changes in the input and on value change will update the product quantity
- **Note:** `data-row-id="{{ $item->rowId }}"` attribute is needed on update input's and remove button's closest `<tr>` in order to update DOM successfully after the change

#### Destroy cart
- Simply add an empty `data-destroy-cart` attribute to your link and all is done

## Order data
#### Creating inputs
- Once you have your inputs ready, add `data-order-customer-field-name` or `data-order-delivery-field-name` attribute with your desired field name name to each input
- Inputs' values will be automatically gathered on form submit

#### Validating inputs
- To validate your inputs, add `data-order-rule` attribute to them
- Supported attribute values are `email`, `phone`, `number`, `filled`, `bool` or your custom regex
- The form is validated before submitting - when validation fails, `has-error` class is added to input's parent

### Shipping/payment methods filtering based on country/shipping method value
- What you most certainly want to do in your cart is to show/hide payment methods based on selected shipping method, and similarily with shipping methods and selected coutnry
- To do this, follow simple instructions:
	* Add an empty option to each select, something like `<option value="" data-filter-values="">Select shipping method</option>`
	* Add `data-order-delivery-field-name` attribute to your selects, with `"country"` value for your contry select, `"shipping"` value for your shipping method select and `"payment"` value for your payment method select
	* Add `data-filter-values` to your country and shipping select, with supported shipping/payment methods' IDs separated by coma
		* This can be done like `data-filter-values="{{ implode(",", $country->shippingMethodsIds) }}"` or `data-filter-values="{{ implode(",", $shipping->paymentsIds) }}"`
	* Next, make sure that options in each select have `value` same as the option's ID
- That's it! Your selects will now hide/show options automatically

#### Submitting data form
- All you need to do is to add `data-submit-order-customer-data` or `data-submit-order-delivery-data` to your button - the controller will validate and submit the form on click

#### Submitting the order
- To submit the whole order, simply add `data-submit-order` attribute to your button - that's it!

## Event listeners
- You can add your own callback functions to be called when some request's state changes
- Callback functions objects are accessible through `window.shop.on.{request name}.{event name}` object
- Available request names: 
	* `request` for all requests
	* `addRequest`, `updateRequest`, `removeRequest`, `destroyRequest` for cart requests
	* `customerDataRequest`, `deliveryDataRequest`, `submitOrderRequest` for order requests
- All requests support `start`, `end`, `done` and `fail` events, `request`, `customerDataRequest` and `deliveryDataRequest` also support `validationDone` and `validationFail` events
- Some events have default methods ready to be used with no coding needed
- See `shop::frontend.partials.cart-ajax.blade.php` for more details

## Custom alert
- Most requests call an alert function when they end
- You can customize your alerts through replacing `window.shop.alert(string, type)` function with your own

## Loading element
- By default, requests toggle $(".loading") element's visibility on their start/end
- You can customize this element through replacing `window.shop.loadinElement` attribute with your custom jQuery object

## Cart quantity/price change
- Everytime a cart content is changed, you might want to update price/products quantity somewhere on your site
- Use attributes `data-cart-insert="items"` and `data-cart-insert="price"` at the elements you want to insert quantity/price to and the controller will replace their content automatically each time cart content is changes

## Price formatting and filtering select values
- If you need to format number as price anywhere through your app, use PHP helper `{{ price($number) }}` or JavaScript function `window.shop.price(number)` - decimal places, delimiters and currency can be set in module's settings, see **Settings** part of README
- If you need to filter select's options based on different select's value (see **Order data > Shipping/payment methods filtering based on country/shipping method value** in README), you can use JS function `window.shop.filterSelectValues(sourceSelect, targetSelect, sourceKeyAttribute?, targetKeyAttribute?, delimiter?)`
	* Simply call it once and the method will listen to selects' changes and toggle options automatically
	* To learn more about function parameters, see `\Modules\Shop\Resources\views\frontend\partials\cart-ajax.blade.php:151`

## Example views
- To see example routes, controller and views, head to `\Modules\Shop\Http\frontendRoutes.php`, `\Modules\Shop\Http\Controllers\Frontend\CartController.php` and `\Modules\Shop\Resources\views\frontend` where fully functional shop prototype is ready

# Cart manipulation (without JS controller)
- To manipulate content of cart, you can either use Facade class `Modules\Shop\Facades\Cart` or AJAX API.

### AJAX API
- API is available at POST `shop-api/cart/(action)` where `(action)` is `insert`, `update`, `remove` or `destroy`
- Passed attributes are same at methods' parameters in the Facade (see bellow)

### PHP Facade
- You can call static methods in `Modules\Shop\Facades\Cart` to manipulate cart content
- For further documentation, see `Modules\Shop\Facades\Cart.php`

# Order submitting (without JS controller)
- Facade `Modules\Shop\Facades\Order` and corresponding AJAX API is ready to help you with orders

### Storing user and delivery data
- When you have multi-page cart, Shop module can store your user data and delivery data so you don't need to
- This can be done through Facade's `storeUserData` and `storeDeliveryData` methods or `api.shop.order.store user data`, `api.shop.order.store delivery data` and `api.shop.order.store order data` API routes.
- Once your data is set, you can retrieve it by `getStoredUserData` and `getStoredDeliveryData` methods

### Submitting order
- Submitting order is done through `submitOrder` method or `api.shop.order.submit` API route
- You can submit your user, delivery and products data at this point but if you're using Cart and store user and delivery data, you don't need to pass any data and all data is taken automatically from Cart and stored data.