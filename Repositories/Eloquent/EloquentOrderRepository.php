<?php

namespace Modules\Shop\Repositories\Eloquent;

use Modules\Shop\Repositories\OrderRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentOrderRepository extends EloquentBaseRepository implements OrderRepository
{
}
