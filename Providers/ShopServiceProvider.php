<?php

namespace Modules\Shop\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Shop\Events\Handlers\RegisterShopSidebar;

class ShopServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterShopSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('orders', array_dot(trans('shop::orders')));
            $event->load('cartcountries', array_dot(trans('shop::cartcountries')));
            $event->load('shippingmethods', array_dot(trans('shop::shippingmethods')));
            $event->load('paymentmethods', array_dot(trans('shop::paymentmethods')));
            $event->load('products', array_dot(trans('shop::products')));
            $event->load('settings', array_dot(trans('shop::settings')));
            // append translations







        });

        require_once app_path()."/../Modules/Shop/Http/helpers.php";
    }

    public function boot()
    {
        $this->publishConfig('shop', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        if (\Schema::hasTable('dynamicpages__templates') && \Schema::hasTable('dynamicpages__template_translations'))
            \Modules\Shop\Entities\Product::createTemplate();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Shop\Repositories\OrderRepository',
            function () {
                $repository = new \Modules\Shop\Repositories\Eloquent\EloquentOrderRepository(new \Modules\Shop\Entities\Order());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Shop\Repositories\Cache\CacheOrderDecorator($repository);
            }
        );
// add bindings







    }
}
