<?php 

return [
	"alerts" => [
		"add done" => "Product successfully added to cart!",
		"update done" => "Product quantity successfully updated!",
		"remove done" => "Product successfully removed from cart!",
		"destroy done" => "Cart content successfully removed!",
		"submit order done" => "Order successfully submitted!",
		"validation fail" 	=> "Form contains errors!"
	]
];