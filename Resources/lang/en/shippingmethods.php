<?php

return [
	"title" => "Shipping methods",
	"title_s" => "Shipping method",
    "form" => [
        "name" => "Name",
        "note"	=> "Note",
        "price" => "Price",
        "active" => "Active"
    ],
    "countries" => [
    	"title" => "Delivery countries",
    	"edit"	=> "Edit delivery countries",
    ],
    "payments" => [
    	"title" => "Payment methods",
    	"edit"	=> "Edit payment methods",
    ],
];
