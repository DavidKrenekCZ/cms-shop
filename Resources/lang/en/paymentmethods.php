<?php

return [
	"title" => "Payment methods",
	"title_s" => "Payment method",
    "form" => [
        "name" => "Name",
        "note"	=> "Note",
        "price" => "Price"
    ],
];
