<?php

return [
	'title' => 'Orders',
	'title_s' => 'Order',
	'table' => [
		'date' => 'Date',
		'customer' => 'Customer',
		'price' => 'Price',
		'done' => 'Finished',
		'filter' => [
			'done' => 'Show only finished',
			'undone' => 'Show only not finished',
			'clear' => 'Show all'
		]
	],
	'button' => [
		'mark done' => 'Mark as finished',
		'mark undone' => 'Mark as not finished',
		'show empty fields' => 'Show blank fields'
	],
	'detail' => [
		'title' => 'Order detail',
		'user data title' => 'Customer data',
		'shipping data title' => 'Shipping data',
		'order data title' => 'Order data',
		'products data title' => 'Products',
		'delivery price' => 'Total delivery price',

		'total price' => 'Total price',
		'price per piece' => 'Price per piece',
		'product quantity' => 'Quantity',
		'product thumbnail' => 'Thumbnail',
		'product name' => 'Name',
		'product code' => 'Code',

		'products' => 'Products',
		'delivery' => 'Delivery',

		'permanently deleted' => 'permanently deleted',
		'deleted' => 'deleted',

		'deleted disclamer' => 'This product has been deleted - a recorvered copy is shown and it is possible that some data could be missing. For more details look for Dynamic Pages Record with ID :id.', /* Tento produkt byl odstraněn a proto je možné, že některá data již nepůjdou zobrazit. Aktuálně se zobrazuje obnovená kopie produktu. Pro více informací hledejte Dynamic Pages Record s ID {{ $item->id }} */
		'permanently deleted disclamer' => 'This product has been permanently deleted and nothing except for name could be restored.'
	],
	'form' => [
	],
	'messages' => [
	],
	'validation' => [
	],
];
