<?php 
return [
	"name" 			=> "Name",
	"surname" 		=> "Surname",
	"phone" 		=> "Phone",
	"email" 		=> "E-mail",
	"address" 		=> "Address",
	"city" 			=> "City",
	"town" 			=> "Town",
	"postal_code"	=> "Postal code",
	"psc" 			=> "Postal code",
	"zip" 			=> "Postal code",
	"country" 		=> "Country",
	"note" 			=> "Note"
];