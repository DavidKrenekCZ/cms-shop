<?php

return [
	"title" => "Delivery countries",
	"title_s" => "Delivery country",
    "form" => [
        "short" => "Short name",
        "name"	=> "Name",
    ],
];
