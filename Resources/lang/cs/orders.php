<?php

return [
	'title' => 'Objednávky',
	'title_s' => 'Objednávka',
	'table' => [
		'date' => 'Datum',
		'customer' => 'Zákazník',
		'price' => 'Cena',
		'done' => 'Hotovo',
		'filter' => [
			'done' => 'Zobrazit pouze hotové',
			'undone' => 'Zobrazit pouze nehotové',
			'clear' => 'Zobrazit vše'
		]
	],
	'button' => [
		'mark done' => 'Označit objednávku jako hotovou',
		'mark undone' => 'Označit objednávku jako nehotovou',
		'show empty fields' => 'Zobrazit prázdná pole'
	],
	'detail' => [
		'title' => 'Detail objednávky',
		'user data title' => 'Osobní údaje',
		'shipping data title' => 'Způsob doručení',
		'order data title' => 'Podrobnosti objednávky',
		'products data title' => 'Produkty',
		'delivery price' => 'Celková cena za doručení',

		'total price' => 'Celková cena',
		'price per piece' => 'Cena za kus',
		'product quantity' => 'Množství',
		'product thumbnail' => 'Náhled',
		'product name' => 'Název',
		'product code' => 'Kód',

		'products' => 'Produkty',
		'delivery' => 'Doručení',

		'permanently deleted' => 'trvale odstraněno',
		'deleted' => 'odstraněno',

		'deleted disclamer' => 'Tento produkt byl odstraněn a zobrazuje se obnovená kopie, proto je možné, že některá data již nepůjdou zobrazit. Pro více informací hledejte Dynamic Pages Record s ID :id.',
		'permanently deleted disclamer' => 'Tento produkt byl trvale odstraněn a nic kromě jména se nepodařilo obnovit.'
	],
	'form' => [
	],
	'messages' => [
	],
	'validation' => [
	],
];
