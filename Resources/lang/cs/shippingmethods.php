<?php

return [
	"title" => "Dopravci",
	"title_s" => "Dopravce",
    "form" => [
        "name" => "Název",
        "note"	=> "Poznámka",
        "price" => "Cena",
        "active" => "Aktivní"
    ],
    "countries" => [
    	"title" => "Země doručení",
    	"edit"	=> "Upravit země doručení",
    ],
    "payments" => [
    	"title" => "Platební metody",
    	"edit"	=> "Upravit platební metody",
    ],
];
