<?php 
return [
	"name" 			=> "Jméno",
	"surname" 		=> "Příjmení",
	"phone" 		=> "Telefon",
	"email" 		=> "E-mail",
	"address" 		=> "Adresa",
	"city" 			=> "Město",
	"town" 			=> "Město",
	"postal_code"	=> "PSČ",
	"psc" 			=> "PSČ",
	"zip" 			=> "PSČ",
	"country" 		=> "Země",
	"note" 			=> "Poznámka"
];