<?php 

return [
	"alerts" => [
		"add done" => "Produkt úspěšně vložen do košíku!",
		"update done" => "Množství produktu úspěšně upraveno!",
		"remove done" => "Produkt úspěšně odstraněn z košíku!",
		"destroy done" => "Obsah košíku úspěšně odstraněn!",
		"submit order done" => "Objednávka úspěšně odeslána, děkujeme!",
		"validation fail" 	=> "Formulář obsahuje chyby!"
	]
];