            <div class="box box-primary">
                <div class="box-header with-border">
            		<h3 class="box-title">{{ trans("shop::shippingmethods.title") }}</h3>
          		</div>
            	<div class="box-body table-responsive edit-toggle">

                    @include('partials.form-tab-headers', [ "prefix" => "shipping" ])
                    @php $i = 0; @endphp
                    {!! Form::open(['route' => ['admin.shop.shippingmethods.store'], 'method' => 'post', 'data-form-name' => 'shipping']) !!}
                    <div class="tab-content">
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        @php $i++; @endphp
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" data-locale="{{ $locale }}" id="tab_shipping_{{ $i }}">

            		  		<table class="table table-striped table-bordered text-center static-table edit-table dataTables_wrapper" data-order-style="asc">
                        		<thead>
                    				<tr>
                                		<th>{{ trans("shop::shippingmethods.form.name") }}</th>
                                		<th>{{ trans("shop::shippingmethods.form.note") }}</th>
                                		<th>{{ trans("shop::shippingmethods.form.price") }}</th>
                  						<th>{{ trans('core::core.table.actions') }}</th>
               	 					</tr>
                        		</thead>
                        		<tbody>
                    				@foreach($shippings as $shipping)
                					<tr class="editable" data-edit-url="{{ route("admin.shop.shippingmethods.edit", [ "id" => $shipping->id ]) }}" data-shipping-id="{{ $shipping->id }}">
                						<td>
                                            <input type="text" value="{{ $shipping->translate($locale)->name }}" data-name="{{ $locale }}:name">
                                            <span>
                                                {{ $shipping->translate($locale)->name }}
                                            </span>
                                        </td>
                            		    <td>
                                            <input type="text" value="{{ $shipping->translate($locale)->note }}" data-name="{{ $locale }}:note"> 
                                            <span>
                                                {{ $shipping->translate($locale)->note }}
                                            </span>
                                        </td>
                                        <td>
                                            <input type="text" value="{{ $shipping->price }}" data-name="price" data-keep-same="payment-price-input-{{ $shipping->id }}" >
                                            <span data-keep-same="payment-price-span-{{ $shipping->id }}">
                                                {{ $shipping->price }}
                                            </span>
                                        </td>
                                		<td>
                                    		<a href="javascript:void(0)" title="{{ trans('shop::shippingmethods.countries.edit') }}" data-toggle="modal" data-target="#sm-countries-modal" data-id="{{ $shipping->id }}" data-checkbox-data="{{ implode(",", $shipping->countriesIds) }}">
                                                <i class="fa fa-globe"></i>
                                            </a>
                                    		&nbsp;&nbsp;
                                    		<a href="javascript:void(0)" title="{{ trans('shop::shippingmethods.payments.edit') }}" data-toggle="modal" data-target="#sm-payments-modal" data-id="{{ $shipping->id }}" data-checkbox-data="{{ implode(",", $shipping->paymentsIds) }}">
                                                <i class="fa fa-dollar"></i>
                                            </a>
                                    		&nbsp;&nbsp;
                                    		<a href="{{ route("admin.shop.shippingmethods.destroy", [ "id" => $shipping->id ]) }}" title="{{ trans('core::core.button.delete') }}" data-link-modal="#delete-modal">
                                                <i class="fa fa-close"></i>
                                            </a>
                                		</td>
                					</tr>
                    				@endforeach
                					<tr>
                                		<td>
                                	    	{!! Form::i18nInput("name", trans('shop::shippingmethods.form.name'), $errors, $locale) !!}
                               		    </td>
                               		 	<td>
		                                    {!! Form::i18nInput("note", trans('shop::shippingmethods.form.note'), $errors, $locale) !!}
        		                        </td>
                                        <td>
                                            {!! Form::normalInput('price', trans('shop::paymentmethods.form.price'), $errors, null, [
                                                "data-keep-same" => "shippingmethods-new-price-input"
                                            ]) !!}
                                        </td>
               		                   	<td>
                       		                <button type="submit" title="Uložit" class="toggle-edit no-button"><i class="fa fa-check" aria-hidden="true"></i></button>
                               		    </td>
            		    			</tr>
                        		</tbody>
            				</table>
                        </div>
                    @endforeach
                    </div>
                    {!! Form::close() !!}
          		</div>
                <div class="overlay loading">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
        	</div>