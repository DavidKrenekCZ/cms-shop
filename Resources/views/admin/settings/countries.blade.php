            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans("shop::cartcountries.title") }}</h3>
          		</div>
            	<div class="box-body table-responsive edit-toggle">
                    @include('partials.form-tab-headers', [ "prefix" => "countries" ])
                    @php $i = 0; @endphp
                    {!! Form::open(['route' => ['admin.shop.cartcountry.store'], 'method' => 'post', 'data-form-name' => 'countries']) !!}
                    <div class="tab-content">
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                    	@php $i++; @endphp
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" data-locale="{{ $locale }}" id="tab_countries_{{ $i }}">
            	    		<table class="table table-striped table-bordered text-center static-table edit-table dataTables_wrapper" data-order-style="asc">
                        		<thead>
                    				<tr>
                    					<th>{{ trans("shop::cartcountries.form.short") }}</th>
                            		    <th>{{ trans("shop::cartcountries.form.name") }}</th>
                  						<th>{{ trans('core::core.table.actions') }}</th>
                					</tr>
                        		</thead>
                        		<tbody>
                        
                    			@foreach($countries as $country)
                					<tr class="editable" data-edit-url="{{ route("admin.shop.cartcountry.edit", [ "id" => $country->id ]) }}">
                                        <td>
                                            <input type="text" value="{{ $country->translate($locale)->short }}" data-name="{{ $locale }}:short">
                                            <span>
                                                {{ $country->translate($locale)->short }}
                                            </span>
                                        </td>
                                        <td>
                                            <input type="text" value="{{ $country->translate($locale)->name }}" data-name="{{ $locale }}:name"> 
                                            <span>
                                                {{ $country->translate($locale)->name }}
                                            </span>
                                        </td>
                            		    <td>
                            		        <a href="{{ route("admin.shop.cartcountry.destroy", [ "cartcountry" => $country->id ]) }}" title="{{ trans('core::core.button.delete') }}" data-link-modal="#delete-modal"><i class="fa fa-close"></i></a>
                            		    </td>
                					</tr>
                    			@endforeach
                					<tr data-id="new" data-type="color">
                                	    <td>
                                	    	{!! Form::i18nInput("short", trans('shop::cartcountries.form.short'), $errors, $locale) !!}
                               		    </td>
                               		 	<td>
		                                    {!! Form::i18nInput("name", trans('shop::cartcountries.form.name'), $errors, $locale) !!}
        		                        </td>
               		                   	<td>
                       		                <button type="submit" title="Uložit" class="toggle-edit no-button"><i class="fa fa-check" aria-hidden="true"></i></button>
                               		    </td>
                					</tr>
                        		</tbody>
            				</table>
                    	</div>
                    @endforeach
                    </div>
                    {!! Form::close() !!}
          		</div>
                <div class="overlay loading">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
        	</div>