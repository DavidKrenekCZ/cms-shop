    <div class="modal table-form-modal fade" id="sm-payments-modal">
    	<div class="modal-dialog">
        	<div class="modal-content">
        		<div class="modal-header">
          			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            			<span aria-hidden="true">&times;</span>
                    </button>
          			<h4 class="modal-title">{{ trans('shop::shippingmethods.payments.edit') }}</h4>
        		</div>
        		<div class="modal-body">
            	    <table class="table table-striped table-bordered text-center static-table dataTables_wrapper" data-order-style="asc">
                        <thead>
                    		<tr>
                    			<th>{{ trans("shop::paymentmethods.form.name") }}</th>
                  				<th>{{ trans('core::core.table.actions') }}</th>
                			</tr>
                        </thead>
                        <tbody>
                        
                    	@foreach($payments as $payment)
                			<tr>
                				<td>
                        	        {{ $payment->name }}
                                </td>
                       		    <td>
                       		        <input type="checkbox" data-id="{{ $payment->id }}">
                       		    </td>
                			</tr>
                    	@endforeach
                   		</tbody>
           			</table>
        		</div>
        		<div class="modal-footer">
          			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ trans('core::core.button.cancel') }}</button>
            		<button type="button" class="btn btn-success btn-confirmed modal-submit" data-edit-url="{{ route("admin.shop.shippingmethods.edit.payments") }}">{{ trans('core::core.button.update') }}</button>
        		</div>
      		</div>
      		<div class="overlay loading">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
      	</div>
	</div>