@extends('layouts.master')

@section('content-header')
	<h1>
		{{ trans('shop::settings.title') }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
		<li class="active">{{ trans('shop::settings.title') }}</li>
	</ol>
@stop

@section("content")
	<div class="row">
		<div class="col-md-6">
			@include("shop::admin.settings.countries")
		</div>

		<div class="col-md-6">
			@include("shop::admin.settings.payment-methods")
		</div>

		<div class="col-md-12">
			@include("shop::admin.settings.shipping-methods")
		</div>
	</div>

	@include("shop::admin.settings.sm-countries-modal")

	@include("shop::admin.settings.sm-payments-modal")

	<!-- --- DELETE MODAL --- -->
	<div class="modal modal-danger fade" id="delete-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">{{ trans("core::core.modal.title") }}</h4>
				</div>
				<div class="modal-body">
					<p>{{ trans("core::core.modal.confirmation-message") }}</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">{{ trans("core::core.button.cancel") }}</button>
					<button type="button" class="btn btn-danger btn-confirmed">{{ trans("core::core.button.delete") }}</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('css-stack')
	<style type="text/css">
		.edit-table td .form-group {
			margin: 0;
		}

		.edit-table td .form-group label {
			display: none;
		}

		.edit-table td button {
			background: none;
			border: none;
		}

		.edit-table td i.fa {
			color: #337ab7;
			font-size: 2rem;
		}

		.edit-table .editable td:not(:last-child) {
			padding: 0;
		}

		.edit-table .editable td span {
			padding: 9px;
			display: inline-block;
		}

		.edit-table .editable td input {
			display: none;
			padding: 7px 15px;
			width: 100%;
			text-align: center;
		}

		.edit-table .editable td input:focus + span {
			display: none;
		}

		.edit-table .editable td input:focus {
			display: initial !important;
		}

		.edit-table .editable td:hover span {
			display: none;
		}

		.edit-table .editable td:hover input {
			display: initial;
		}

		.table-form-modal .modal-body {
			padding: 0;
		}

		.table-form-modal .modal-body>table {
			margin: 0;
		}

		.modal .loading {
			position: absolute;
			display: none;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background: rgba(255, 255, 255, 0.7);
		}

		.modal .loading .fa {
			position: absolute;
			top: 50%;
			left: 50%;
			margin-left: -15px;
			margin-top: -15px;
			color: #000;
			font-size: 30px;
		}
	</style>
@endpush

@push('js-stack')
	<?php $locale = locale(); ?>
	<script type="text/javascript">
		$(function () {
			// Show only errors for form that has been submitted
			if (document.location.href.indexOf("#form-") > -1) {
				var submittedForm = document.location.href.split("#form-")[1];

				// Hide errors from not-submitted forms
				$("form").each(function() {
					var name = $(this).data("form-name");
					if (name && name != submittedForm) {
						$(this).find(".has-error .help-block").remove();
						$(this).find(".has-error").removeClass("has-error");
					}
				});

				setTimeout(function() {
					$(".box .loading").fadeOut()
				}, 200);
			} else
				$(".box .loading").fadeOut();

			// Initialize iCheck
			$('input[type=checkbox]').iCheck({
				checkboxClass: 'icheckbox_flat-blue',
	  			radioClass: 'iradio_flat-blue'
			});

			// Show confirm modal before hitting a link
			$("a[data-link-modal]").click(function(e) {
				e.preventDefault();
				$($(this).data("link-modal")).modal('show')
					.find(".btn-confirmed").attr("onclick", "window.location.href = '"+$(this).attr("href")+"'");
				return false;
			});

			// Keep values/texts of elements the same
			$("[data-keep-same]").on("keyup", function() {
				var val = $(this).val();
				$("[data-keep-same='"+$(this).data("keep-same")+"']").val(val).text(val);
			});

			$('.data-table').dataTable({
				"paginate": true,
				"lengthChange": true,
				"filter": true,
				"sort": true,
				"info": true,
				"autoWidth": true,
				"order": [[ 0, "desc" ]],
				"language": {
					"url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
				}
			});

			// Submit edit form on Enter hit / real blur of input
			window.blurIsArtificial = false;
			$(".editable td input").on({
				keydown: function(e) {
					if (e.key == "Enter") {
						e.stopPropagation();
						e.preventDefault();

						submitEditableField($(this));

						return false;
					}
				},
				blur: function(e) {
					if (!window.blurIsArtificial)
						submitEditableField($(this));
					window.blurIsArtificial = false;
				}
			});

			// Table checkbox form was shown - check/uncheck checkboxes
			$(".table-form-modal").on("show.bs.modal", function(e) {
				var link = $(e.relatedTarget);

				// Check active checkboxes
				var data = link.data("checkbox-data").toString().split(",");
				$(this).find("input[type='checkbox']").iCheck('uncheck');
				for (var index in data)
					$(this).find("input[type='checkbox'][data-id='"+data[index]+"']").iCheck('check');

				var shippingId = link.closest("[data-shipping-id]").data("shipping-id");
				$(this).find(".modal-submit").data("shipping-id", shippingId);
			});

			// Table form was submitted
			$(".table-form-modal .modal-submit").click(function() {
				var modal = $(this).closest(".modal");
				var shippingId = $(this).data("shipping-id");

				// Get checkboxes data
				var checkedIds = [];
				$(this).closest(".modal").find("input[type='checkbox'][data-id]:checked").each(function() {
					checkedIds.push($(this).data("id"));
				});

				var url = $(this).data("edit-url");
				var loading = $(this).closest(".modal").find(".loading");

				loading.show();
				$.ajax({
					url: url,
					type: "POST",
					data: {
						shippingId: shippingId,
						checkedIds: checkedIds,
						_token: "{{ csrf_token() }}"
					}
				}).done(function(d) {
					if (d && d.ok && typeof d.ids == "string") {
						// "Reload" data
						$(".editable[data-shipping-id='"+shippingId+"']").find("[data-checkbox-data][data-target='#"+modal.attr("id")+"']").data("checkbox-data", d.ids);
						loading.closest(".modal").modal("hide");
					} else
						alert("{{ trans("core::core.something went wrong") }}");
				}).fail(function(d) {
					alert("{{ trans("core::core.something went wrong") }}");
				}).always(function() {
					loading.hide();
				});
			});
		});

		// Submit editable field
		function submitEditableField(input) {
			window.blurIsArtificial = true;
			input.blur();

			var inputVal = input.val().trim();
			var span = input.parent().find("span");
			var spanVal = span.text().trim()

			// Value has changed
			if (inputVal != spanVal) {
				var url = input.closest(".editable").data("edit-url");
				var name = input.data("name");

				var loading = input.closest(".box").find(".loading");
				loading.show();

				$.ajax({
					url: url,
					type: "POST",
					data: {
						name: name,
						value: inputVal,
						_token: "{{ csrf_token() }}"
					}
				}).done(function(d) {
					if (d && d.ok) {
						span.text(inputVal);
						$("[data-keep-same='"+span.data("keep-same")+"']").text(inputVal).val(inputVal);
					} else
						alert("{{ trans("core::core.something went wrong") }}");
				}).fail(function() {
					alert("{{ trans("core::core.something went wrong") }}");
				}).always(function() {
					loading.hide();
				});
			}
		}
	</script>
@endpush
