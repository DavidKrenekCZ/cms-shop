            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans("shop::paymentmethods.title") }}</h3>
          		</div>
            	<div class="box-body table-responsive edit-toggle">
                    @include('partials.form-tab-headers', [ "prefix" => "payment" ])
                    @php $i = 0; @endphp
                    {!! Form::open(['route' => ['admin.shop.paymentmethods.store'], 'method' => 'post', 'data-form-name' => 'payment']) !!}
                    <div class="tab-content">
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                    	@php $i++; @endphp
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" data-locale="{{ $locale }}" id="tab_payment_{{ $i }}">
            	    		<table class="table table-striped table-bordered text-center static-table edit-table dataTables_wrapper" data-order-style="asc">
                        		<thead>
                    				<tr>
                    					<th>{{ trans("shop::paymentmethods.form.name") }}</th>
                            		    <th>{{ trans("shop::paymentmethods.form.note") }}</th>
                                        <th>{{ trans("shop::paymentmethods.form.price") }}</th>
                  						<th>{{ trans('core::core.table.actions') }}</th>
                					</tr>
                        		</thead>
                        		<tbody>
                        
                    			@foreach($payments as $payment)
                					<tr class="editable" data-edit-url="{{ route("admin.shop.paymentmethods.edit", [ "id" => $payment->id ]) }}">
                						<td>
                                            <input type="text" value="{{ $payment->translate($locale)->name }}" data-name="{{ $locale }}:name">
                                            <span>
                                                {{ $payment->translate($locale)->name }}
                                            </span>
                                        </td>
                            		    <td>
                                            <input type="text" value="{{ $payment->translate($locale)->note }}" data-name="{{ $locale }}:note"> 
                                            <span>
                                                {{ $payment->translate($locale)->note }}
                                            </span>
                                        </td>
                                        <td>
                                            <input type="text" value="{{ $payment->price }}" data-name="price" data-keep-same="payment-price-input-{{ $payment->id }}">
                                            <span data-keep-same="payment-price-span-{{ $payment->id }}">
                                                {{ $payment->price }}
                                            </span>
                                        </td>
                            		    <td>
                            		        <a href="{{ route("admin.shop.paymentmethods.destroy", [ "id" => $payment->id ]) }}" title="{{ trans('core::core.button.delete') }}" data-link-modal="#delete-modal"><i class="fa fa-close"></i></a>
                            		    </td>
                					</tr>
                    			@endforeach
                					<tr>
                                	    <td>
                                	    	{!! Form::i18nInput("name", trans('shop::paymentmethods.form.name'), $errors, $locale) !!}
                               		    </td>
                               		 	<td>
		                                    {!! Form::i18nInput("note", trans('shop::paymentmethods.form.note'), $errors, $locale) !!}
        		                        </td>
                                        <td>
                                            {!! Form::normalInput('price', trans('shop::paymentmethods.form.price'), $errors, null, [
                                                "data-keep-same" => "paymentmethods-new-price-input"
                                            ]) !!}
                                        </td>
               		                   	<td>
                       		                <button type="submit" title="Uložit" class="toggle-edit no-button"><i class="fa fa-check" aria-hidden="true"></i></button>
                               		    </td>
                					</tr>
                        		</tbody>
            				</table>
                    	</div>
                    @endforeach
                    </div>
                    {!! Form::close() !!}
          		</div>
                <div class="overlay loading">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
        	</div>