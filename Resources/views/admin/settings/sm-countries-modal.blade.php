    <div class="modal table-form-modal fade" id="sm-countries-modal">
    	<div class="modal-dialog">
        	<div class="modal-content">
        		<div class="modal-header">
          			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            			<span aria-hidden="true">&times;</span>
                    </button>
          			<h4 class="modal-title">{{ trans('shop::shippingmethods.countries.edit') }}</h4>
        		</div>
        		<div class="modal-body">
            	    <table class="table table-striped table-bordered text-center static-table dataTables_wrapper" data-order-style="asc">
                        <thead>
                    		<tr>
                    			<th>{{ trans("shop::cartcountries.form.name") }}</th>
                  				<th>{{ trans('shop::shippingmethods.form.active') }}</th>
                			</tr>
                        </thead>
                        <tbody>
                        
                    	@foreach($countries as $country)
                			<tr>
                				<td>
                        	        {{ $country->name }}
                                </td>
                       		    <td>
                       		        <input type="checkbox" data-id="{{ $country->id }}">
                       		    </td>
                			</tr>
                    	@endforeach
                   		</tbody>
           			</table>
        		</div>
        		<div class="modal-footer">
          			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ trans('core::core.button.cancel') }}</button>
            		<button type="button" class="btn btn-success btn-confirmed modal-submit" data-edit-url="{{ route("admin.shop.shippingmethods.edit.countries") }}">{{ trans('core::core.button.update') }}</button>
        		</div>
      		</div>
      		<div class="overlay loading">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
      	</div>
	</div>