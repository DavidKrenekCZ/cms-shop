@extends('layouts.master')

@section('content-header')
	<h1>
		{{ trans('shop::orders.title') }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
		<li class="active">{{ trans('shop::orders.title') }}</li>
	</ol>
@stop

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
						<table class="data-table table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>{{ trans("shop::orders.table.date") }}</th>
									<th>{{ trans("shop::orders.table.customer") }}</th>
									<th>{{ trans("shop::orders.table.price") }}</th>
									<th>
										{{ trans("shop::orders.table.done") }}&nbsp;&nbsp;
										<a class="btn btn-default btn-flat active" data-done-filter="false" title="{{ trans("shop::orders.table.filter.clear") }}"><i class="fa fa-trash"></i></a>
										<a class="btn btn-default btn-flat" data-done-filter="undone" title="{{ trans("shop::orders.table.filter.undone") }}"><i class="fa fa-ban"></i></a>
										<a class="btn btn-default btn-flat" data-done-filter="done" title="{{ trans("shop::orders.table.filter.done") }}"><i class="fa fa-check"></i></a>
									</th>
									<th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
								</tr>
							</thead>
							<tbody>

							@foreach ($orders as $order)
								<tr>
									<td>{{ $order->id }}</td>
									<td>{{ $order->created_at->format("d. m. Y H:i:s") }}</td>
									<td>{{ $order->user->name }}</td>
									<td>{{ price($order->price) }}</td>
									<td>
										<span class="hide insert-action">@if(!$order->done){{ "un" }}@endif{{ "done" }}</span>
										<i class="fa fa-check toggle-hide green no-color @if(!$order->done) hide @endif"></i>
										<i class="fa fa-times toggle-hide red no-color @if($order->done) hide @endif"></i>
									</td>
									<td>
										<div class="btn-group" data-id="{{ $order->id }}">
											<a href="{{ route('admin.shop.order.detail', [$order->id]) }}" class="btn btn-default btn-flat" title="{{ trans("shop::orders.detail.title") }}"><i class="fa fa-eye"></i></a>
											
											<a href="#" class="mark-done toggle-hide @if (!$order->done) hide @endif btn btn-default btn-flat" data-action="undone" title="{{ trans("shop::orders.button.mark undone") }}"><i class="fa fa-ban"></i></a>

											<a href="#" class="mark-done toggle-hide @if ($order->done) hide @endif btn btn-default btn-flat" data-action="done" title="{{ trans("shop::orders.button.mark done") }}"><i class="fa fa-check"></i></a>

											&nbsp;
											<button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.shop.order.destroy', [$order->id]) }}" title="{{ trans("core::core.button.delete") }}"><i class="fa fa-trash"></i></button>
										</div>
									</td>
								</tr>
							@endforeach

							</tbody>
							<tfoot>
								<tr>
									<th>#</th>
									<th>{{ trans("shop::orders.table.date") }}</th>
									<th>{{ trans("shop::orders.table.customer") }}</th>
									<th>{{ trans("shop::orders.table.price") }}</th>
									<th>{{ trans("shop::orders.table.done") }}</th>
									<th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
								</tr>
							</tfoot>
						</table>
						<!-- /.box-body -->
					</div>
				</div>
				<!-- /.box -->

				<div class="overlay loading">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
			</div>
		</div>
	</div>
	@include('core::partials.delete-modal')
@stop

@push('css-stack')
	<style>
		.fa.green, .fa.red {
			font-size: 2rem;
		}

		.fa.green {
			color: green;
		}

		.fa.red {
			color: red;
		}
	</style>
@endpush

@push('js-stack')
	<?php $locale = locale(); ?>
	<script type="text/javascript">
		var dataTablesSettings = {
			"paginate": true,
			"lengthChange": true,
			"filter": true,
			"sort": true,
			"info": true,
			"autoWidth": true,
			"order": [[ 0, "desc" ]],
			"language": {
				"url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
			}
		};
		$(function () {
			$(".box .loading").fadeOut();

			$('.data-table').dataTable(dataTablesSettings);

			$(".mark-done").click(function() {
				var id = $(this).parent().data("id");
				var action = $(this).data("action");
				var that = $(this);

				var loading = $(".loading");
				$(".loading").show();

				$.ajax("{{ route("admin.shop.order.change done state") }}", {
					type: "post",
					data: {
						id: id,
						action: action,
						_token: "{{ csrf_token() }}"
					}
				}).done(function(d) {
					if (d.ok) {
						//alert("{{ trans('core::core.messages.resource updated', ['name' => trans('shop::orders.title_s')]) }}");

						that.closest("tr").find(".toggle-hide").toggleClass("hide"); 	// switch done/undone
						that.closest("tr").find(".insert-action").html(action);  		// add action for sorting
						
						dataTablesSettings.destroy = true;
						$(".data-table").dataTable(dataTablesSettings) // reinit datatable
					} else
						alert("{{ trans("core::core.something went wrong") }}");
				}).fail(function() {
					toastr["error"]("{{ trans("core::core.something went wrong") }}");
				}).always(function(d) {
					$(".loading").hide();
				});

				return false;
			});

			// Filter was changed
			$("[data-done-filter]").click(function() {
				// Already active
				if ($(this).hasClass("active"))
					return false;

				var table = $(this).closest(".data-table");

				// Toggle active class
				$(this).parent().find(".active").removeClass("active");
				$(this).addClass("active");

				$(this).parent().blur(); // otherwise blue outline occurs

				// Redraw table
				table.DataTable().draw();

				return false;
			});
		});

		$.fn.dataTable.ext.search.push(
		    function (settings, data, dataIndex) {
		    	var table = $(settings.nTable);
		    	var filterButton = table.find(".active[data-done-filter]");

		        var filter = filterButton.data("done-filter");
		        if (!filter)									// clear filter
		        	return true;

				var filterIsDone = filter == "done";			// show "done" only

				var colIndex = filterButton.parent().index(); 	// column to be filtered by

		        var value = data[colIndex];						// cell value
		        var isDone = value.indexOf("undone") < 0; 		// row is "done" 

		        return ((isDone && filterIsDone) || (!isDone && !filterIsDone));
		    }
		);
	</script>
@endpush
