@extends('layouts.master')

@section('content-header')
	<h1>
		{{ trans('shop::orders.detail.title') }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
		<li><a href="{{ route('admin.shop.order.index') }}">{{ trans('shop::orders.title') }}</a></li>
		<li class="active">{{ trans('shop::orders.detail.title') }}</li>
	</ol>
@stop

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
	  		<h3 class="box-title">{{ trans("shop::orders.detail.order data title") }}</h3>
		</div>
		<div class="box-body table-responsive">
			<div class="col-sm-12">
				<div class="row order-detail-overview">
					<div class="col-xs-12 col-sm-6">
						<h4>{{ trans("shop::orders.detail.user data title") }}</h4>
						<p class="empty-fields-checkbox">
							<label>
								<input type="checkbox"> {{ trans("shop::orders.button.show empty fields") }}
							</label>
						</p>
						@foreach ($order->userTranslated as $item)
						<div class="col-md-6 @if($item->isEmpty) empty-item hide @endif">
							<p><b>{{ $item->name }}</b>: {{ !$item->isEmpty ? $item->value : "-" }}</p>
						</div>
						@endforeach
		  			</div>
					<div class="col-xs-12 col-sm-6">
						<h4>{{ trans("shop::orders.detail.shipping data title") }}</h4>
						<p><b>{{ trans("shop::cartcountries.title_s") }}</b>: {{ $order->delivery->country->name }}</p>
						<p><b>{{ trans("shop::paymentmethods.title_s") }}</b>: {{ $order->delivery->payment->name }} ({{ price($order->delivery->payment->price) }})</p>
						<p><b>{{ trans("shop::shippingmethods.title_s") }}</b>: {{ $order->delivery->shipping->name }} ({{ price($order->delivery->shipping->price) }})</p>
						<p><b>{{ trans("shop::orders.detail.delivery price") }}</b>: {{ price($order->delivery->price) }}</p>
		  			</div>
				</div>
	  		</div>
		</div>
  	</div>
	<div class="box box-info">
		<div class="box-header with-border">
	  	<h3 class="box-title">{{ trans("shop::orders.detail.products data title") }}</h3>
	</div>
	<div class="box-body table-responsive">
		<h4 class="text-center">
			<b>
				{{ trans("shop::orders.detail.total price") }}: {{ price($order->price) }}
			</b> 
		</h4>
		<h5 class="text-center">
			({{ lcfirst(trans("shop::orders.detail.products")) }} {{ price($order->productsPrice) }}, {{ lcfirst(trans("shop::orders.detail.delivery")) }} {{ price($order->delivery->price) }})
		</h5>
		<br>
		<table class="table table-striped table-bordered text-center data-table" data-order-by="1" data-order-style="asc">
			<thead>
				<tr>
			  		<th>{{ trans("shop::orders.detail.product code") }}</th>
					<th>{{ trans("shop::orders.detail.product thumbnail") }}</th>
					<th>{{ trans("shop::orders.detail.product name") }}</th>
					<th>{{ trans("shop::orders.detail.price per piece") }}</th>
					<th>{{ trans("shop::orders.detail.product quantity") }}</th>
			  		<th>{{ trans("shop::orders.detail.total price") }}</th>
				</tr>
			</thead>
			@foreach($order->products as $item)
				<tr>
					@if (count($item->model))
						{{-- PRODUCT WAS NOT DELETED OR WAS RESTORED --}}
						<td>{{ $item->model->field("Code") != "" ? $item->model->field("Code") : "-" }}</td>
						<td>
							@if ($item->model->mainImage)
							<a href="{{ $item->model->mainImage }}" target="_blank">
								<img src="{{ $item->model->mainImage }}" class="thumbnail">
							</a>
							@else
							-
							@endif
						</td>
						<td>
							@if ($item->deleted)
							{{ $item->model->name or "-" }}
							<br>
							<b title="{{ trans("shop::orders.detail.deleted disclamer", [ "id" => $item->id ]) }}">
								[-{{ trans("shop::orders.detail.deleted") }}-]
							</b>
							@else
							<a href="{{ route("admin.dynamicpages.record.edit", [ "record" => $item->model ]) }}">
								{{ $item->model->name or "-" }}
							</a>
							@endif
						</td>
					@else
						{{-- PRODUCT WAS HARDLY DELETED AND CANNOT BE RESTORED ANYMORE --}}
						<td>-</td>
						<td>-</td>
						<td>
							{{ $item->name }} 
							<br>
							<b title="{{ trans("shop::orders.detail.permanently deleted disclamer") }}">
								[-{{ trans("shop::orders.detail.permanently deleted") }}-]
							</b>
						</td>
					@endif
					<td>{{ price($item->price) }}</td>
					<td>{{ $item->quantity }}</td>
					<td>{{ price($item->price*$item->quantity) }}</td>
				</tr>
			@endforeach
		</table>
	</div>
  </div>    
@stop

@push('css-stack')
	<style type="text/css">
		.empty-fields-checkbox div {
			margin-right: .5rem;
			margin-top: -3px;
		}

		.empty-fields-checkbox label {
			font-weight: 500;
		}

		.order-detail-overview p {
			padding-left: 1rem;
		}

		.thumbnail {
  			width: 80px;
  			height: 80px;
  			margin: 0 auto;
		}
	</style>
@endpush

@push('js-stack')
	<script>
		$( document ).ready(function() {
			$('.data-table').dataTable({
				"paginate": true,
				"lengthChange": true,
				"filter": true,
				"sort": true,
				"info": true,
				"autoWidth": true,
				"order": [[ 0, "desc" ]],
				"language": {
					"url": '<?php echo Module::asset("core:js/vendor/datatables/{locale()}.json") ?>'
				}
			});

			$('input[type="checkbox"], input[type="radio"]').iCheck({
				checkboxClass: 'icheckbox_flat-blue',
				radioClass: 'iradio_flat-blue'
			});

			$(".empty-fields-checkbox input").on("ifToggled", function(e) {
				$(this).closest(".col-xs-12").find(".empty-item").toggleClass("hide", !this.checked);
			});

			if (!$(".order-detail-overview .empty-item").length)
				$(".empty-fields-checkbox").remove();
		});
	</script>
@endpush
