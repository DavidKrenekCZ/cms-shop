@extends("shop::frontend.layout")

@section("content")
<h1>Cart</h1>
<div>
	<a href="javascript:void(0)" data-destroy-cart>Delete all from cart</a><br><br>
	@include("shop::frontend.includes.cart-table", [ "editable" => true ])<br>
	<a href="{{ route("frontend.shop.cart", 2) }}">Continue ></a>
</div>
@endsection