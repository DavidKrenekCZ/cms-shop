@extends("shop::frontend.layout")

@section("content")
<h1>Confirm order</h1>
<div>
	<span class="field-wrapper">
		<b>Name:</b> {{ $userData["name"] or "" }}
	</span>
	<span class="field-wrapper">
		<b>E-mail:</b> {{ $userData["email"] or "" }}
	</span>
	<span class="field-wrapper">
		<b>Phone:</b> {{ $userData["phone"] or "" }}
	</span>
	<span class="field-wrapper">
		<b>Address:</b> {{ $userData["address"] or "" }}
	</span>
	<span class="field-wrapper">
		<b>Town:</b> {{ $userData["town"] or "" }}
	</span>
	<br><br>
	@if ($countries->count() > 1)
	<span class="field-wrapper">
		<b>Country:</b> {{ $deliveryData["countryModel"]->name }}
	</span>
	@endif
	<span class="field-wrapper">
		<b>Shipping method:</b> {{ $deliveryData["shippingModel"]->name }}{!! trim($deliveryData["shippingModel"]->note) != "" ? " - <i>".$deliveryData["shippingModel"]->note : "</i>" !!} ({{ price($deliveryData["shippingModel"]->price) }})
	</span>
	<span class="field-wrapper">
		<b>Payment method:</b> {{ $deliveryData["paymentModel"]->name }}{!! trim($deliveryData["paymentModel"]->note) != "" ? " - <i>".$deliveryData["paymentModel"]->note : "</i>" !!} ({{ price($deliveryData["paymentModel"]->price) }})
	</span><br>
	@include("shop::frontend.includes.cart-table", [ "editable" => false ])
	<br>
	<h3>Total price: {{ price(\Modules\Shop\Facades\Cart::getCartSubtotal(false) + $deliveryData["shippingModel"]->price + $deliveryData["paymentModel"]->price) }}</h3><br>
	<button data-submit-order><h4>Submit order</h4></button><br><br>
	<br>&nbsp;
	<a href="{{ route("frontend.shop.cart") }}">< Edit cart content</a><br><br>&nbsp;
	<a href="{{ route("frontend.shop.cart", 2) }}">< Edit personal data</a><br><br>&nbsp;
	<a href="{{ route("frontend.shop.cart", 3) }}">< Edit delivery data</a>
</div>
@endsection