@extends("shop::frontend.layout")

@section("content")
<h1>Customer data</h1>
<div>
	<span class="field-wrapper">
		<b>Name:</b> <input type="text" value="{{ $userData["name"] or "" }}" data-order-customer-field-name="name" data-order-rule="filled">
	</span>
	<span class="field-wrapper">
		<b>E-mail:</b> <input type="text" value="{{ $userData["email"] or "" }}" data-order-customer-field-name="email" data-order-rule="email">
	</span>
	<span class="field-wrapper">
		<b>Phone:</b> <input type="text" value="{{ $userData["phone"] or "" }}" data-order-customer-field-name="phone" data-order-rule="phone">
	</span>
	<span class="field-wrapper">
		<b>Address:</b> <input type="text" value="{{ $userData["address"] or "" }}" data-order-customer-field-name="address" data-order-rule="^[A-Ža-ž0-9 ,\.]+$">
	</span>
	<span class="field-wrapper">
		<b>Town:</b> <input type="text" value="{{ $userData["town"] or "" }}" data-order-customer-field-name="town" data-order-rule="">
	</span>
	<br>&nbsp;
	<a href="{{ route("frontend.shop.cart") }}">< Back to cart</a>
	@for($x = 0; $x < 100; $x++) &nbsp; @endfor
	<a data-submit-order-customer-data>Continue ></a>
</div>
@endsection