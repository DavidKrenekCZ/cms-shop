@extends("shop::frontend.layout")

@section("content")
<h1>Products</h1>
<div>
	@foreach ($products as $product)
	<div class="product">
		<div>
			<a href="{{ $product->purl("products/") }}">{{ $product->name }}</a><br>
			<b>Code:</b> {{ $product->field("Code") }}<br>
			<b>Price:</b> {{ $product->field("Price") }}<br><br>
			<a href="javascript:void(0)" data-add-to-cart-id="{{ $product->id }}">ADD TO CART</a>
		</div>
		@if ($product->mainImage)
		<a href="{{ $product->purl("products/") }}">
			<img src="{{ $product->mainImage }}">
		</a>
		@endif
	</div>
	@endforeach
</div>
@endsection