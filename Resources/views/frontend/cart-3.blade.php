@extends("shop::frontend.layout")

@section("content")
<h1>Delivery data</h1>
<div>

	@if ($countries->count() > 1)
	<span class="field-wrapper">
		<b>Select country:</b> 
		<select data-order-delivery-field-name="country" data-order-rule="number">
			<option value="" data-filter-values="">Select shipping method</option>
			@foreach ($countries as $country)
			<option value="{{ $country->id }}" data-filter-values="{{ implode(",", $country->shippingMethodsIds) }}" {{ $deliveryData["country"] == $country->id ? "selected" : "" }}>
				{{ $country->name }}
			</option>
			@endforeach
		</select>
	</span>
	@else
	<input type="hidden" data-order-delivery-field-name="country" value="{{ $countries->first()->id }}">
	@endif

	@if ($shippings->count())
	<span class="field-wrapper">
		<b>Select shipping method:</b> 
		<select data-order-delivery-field-name="shipping" data-order-rule="number">
			<option value="" data-filter-values="">Select shipping method</option>
			@foreach ($shippings as $shipping)
			<option value="{{ $shipping->id }}" data-filter-values="{{ implode(",", $shipping->paymentsIds) }}" {{ $deliveryData["shipping"] == $shipping->id ? "selected" : "" }}>
				{{ $shipping->name }}{{ trim($shipping->note) != "" ? " - ".$shipping->note : "" }} ({{ price($shipping->price) }})
			</option>
			@endforeach
		</select>
	</span>
	@endif

	@if ($payments->count())
	<span class="field-wrapper">
		<b>Select payment method:</b> 
		<select data-order-delivery-field-name="payment" data-order-rule="number">
			<option value="">Select payment method</option>
			@foreach ($payments as $payment)
			<option value="{{ $payment->id }}" {{ $deliveryData["payment"] == $payment->id ? "selected" : "" }}>
				{{ $payment->name }}{{ trim($payment->note) != "" ? " - ".$payment->note : "" }} ({{ price($payment->price) }})
			</option>
			@endforeach
		</select>
	</span>
	@endif
	<br>&nbsp;
	<a href="{{ route("frontend.shop.cart", 2) }}">< Back to customer data</a>
	@for($x = 0; $x < 100; $x++) &nbsp; @endfor
	<a data-submit-order-delivery-data>Continue ></a>
</div>
@endsection