	<table class="cart-table">
		<thead>
			<tr>
				<th>Product</th>
				<th>Thumbnail</th>
				<th>Quantity</th>
				<th>Price per piece</th>
				<th>Total price</th>
				@if($editable)<th>Remove</th>@endif
			</tr>
		</thead>
		<tbody>
			@foreach ($cartContent as $item)
			<tr data-row-id="{{ $item->rowId }}">
				<td>{{ $item->name }}</td>
				<td><img src="{{ $item->options->image }}"></td>
				<td>@if($editable)<input type="number" value="{{ $item->qty }}" data-previous-value="{{ $item->qty }}" data-change-quantity-id="{{ $item->rowId }}">@else {{ $item->qty }} @endif</td>
				<td data-price="{{ $item->price }}" class="price">{{ price($item->price) }}</td>
				<td class="total-price">{{ price($item->qty * $item->price) }}</td>
				@if($editable)<td data-remove-from-cart-id="{{ $item->rowId }}"><a href="javascript:void(0)">X</a></td>@endif
			</tr>
			@endforeach
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><b data-cart-insert="price">{{ \Modules\Shop\Facades\Cart::getCartSubtotal() }}</b></td>
				@if($editable)<td></td>@endif
			</tr>
		</tbody>
	</table>