<script type="text/javascript">
	// Customer data filled - redirect to next step
	window.shop.on.customerDataRequest.done = function() {
		document.location = "{{ route("frontend.shop.cart", 3) }}";
	}

	// Redirect to final cart step after delivery data is filled
	window.shop.on.deliveryDataRequest.done = function() {
		document.location = "{{ route("frontend.shop.cart", 4) }}";
	}

	// Redirect back to products after order is submitted
	window.shop.on.submitOrderRequest.done = function() {
		window.shop.alert("{{ trans("shop::cart.alerts.submit order done") }}", "success"); 
		document.location = "{{ route("frontend.shop.products overview") }}"; 
	}
</script>