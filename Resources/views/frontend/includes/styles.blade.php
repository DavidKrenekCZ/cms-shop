		<style type="text/css">
			body {
				padding-top: 3rem;
			}

			nav {
				background: #323232;
				color: white;
				position: fixed;
				top: 0;
				right: 0;
				padding: 1rem .5rem;
				border-bottom-left-radius: 5px;
				box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);
			}

			nav.back-to-products {
				right: auto;
				left: 0;
				border-bottom-left-radius: 0;
				border-bottom-right-radius: 5px;
			}

			nav a {
				color: white;
				text-decoration: none;
			}

			.product {
				float: left;
				width: 25%;
				padding: 1rem;
				box-sizing: border-box;
				box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
				font-family: Arial, Helvetica, sans-serif;
			}

			.product.product-detail {
				width: 100%;
			}

			.product>div {
				float: left;
			}

			.product>div>a {
				color: black;
				font-size: 1.25rem;
				font-weight: bold;
				margin-bottom: 1rem;
				display: inline-block;
			}

			.product img {
				float: right;
				max-width: 50%;
			}

			.cart-table, .cart-table td, .cart-table th {
				border-collapse: collapse;
				border: 1px solid black;
				padding: 5px 10px;
			}

			.cart-table img {
				max-width: 100px;
			}

			.field-wrapper {
				display: block;
				padding: .5rem 1rem;
			}

			.field-wrapper.has-error {
				color: red;
			}

			.field-wrapper.has-error input {
				border: 2px solid red;
			}

			.loading {
				display: none;
				background: rgba(255,255,255,.5);
				color: black;
				position: fixed;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				text-align: center;
				line-height: 100vh;
				font-size: 2rem;
				font-weight: bold;
			}
		</style>