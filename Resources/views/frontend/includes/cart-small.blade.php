@include("shop::frontend.partials.cart-ajax", [ "omit" => [] ])
<nav>
	<a href="{{ route("frontend.shop.cart") }}">
		<b>Cart:</b> <span data-cart-insert="items">{{ \Modules\Shop\Facades\Cart::getCartCount() }}</span> items / <span data-cart-insert="price">{{ \Modules\Shop\Facades\Cart::getCartSubtotal() }}</span>
	</a>
</nav>