		// Format number like in PHP
		_numberFormat: function(number, decimals, dec_point, thousands_sep) {
			// Credits: Umair Hamid, https://stackoverflow.com/questions/12820312/equivalent-to-php-function-number-format-in-jquery-javascript
    		// Strip all characters but numerical ones.
    		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    		var n = !isFinite(+number) ? 0 : +number,
    		    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    		    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    		    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    		    s = '',
    		    toFixedFix = function (n, prec) {
    		        var k = Math.pow(10, prec);
    		        return '' + Math.round(n * k) / k;
    		    };
    		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
    		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    		if (s[0].length > 3)
    		    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    		
    		if ((s[1] || '').length < prec) {
    		    s[1] = s[1] || '';
    		    s[1] += new Array(prec - s[1].length + 1).join('0');
    		}
    		return s.join(dec);
		},

		// Get order data from form
		_getFormData: function(fields, dataNameAttribute, onValidationFail, onValidationDone) {
			onValidationFail = onValidationFail || function() {};
			onValidationDone = onValidationDone || function() {};
			var validationFail = $([]);
			var data = {};
			fields.each(function() {
				var rule = $(this).data("order-rule");
				if (rule)
					rule = rule.toString().trim();
				else
					rule = "";

				var name = $(this).data(dataNameAttribute);
				var val = $(this).val();

				if ($(this).attr("type") == "checkbox")
					val = !!(this.checked);

				var regex;
				switch (rule) {
					case "email": 	regex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"; break;
					case "phone": 	regex = "^\\+{0,1}[0-9 ]{7,16}$"; break;
					case "number": 	regex = "^[0-9]+$"; break;
					case "filled": 	regex = "^.+$"; break;
					case "bool": 	regex = "^(true|false|0|1)$"; break;
					case "": 		regex = "^.*$"; break;
					default: 		regex = rule;
				}

				regex = new RegExp(regex);

				if (!regex.test(val))
					validationFail = validationFail.add($(this));
				else
					data[name] = val;
			});

			if (validationFail.length) {
				onValidationFail($(validationFail));
				window.shop.on.request.validationFail($(validationFail));
				return false;
			}
			onValidationDone();
			window.shop.on.request.validationDone();
			return data;
		},

		// Filter select options based on attribute value
		_filterSelectValues: function(select, values, attribute) {
			select.find("option").each(function() {
				var val = $(this).attr(attribute).trim();
				if (values.indexOf(val) > -1 || val == "")
					$(this).show();
				else
					$(this).hide();
			});

			// Selected option is now hidden - select first option
			if (select.find("option:selected").attr("style") == "display: none;") {
				select.val(select.find("option:first").val());
				select.change();
			}
		}