<script type="text/javascript">
	window.shop = {
		/* --- PUBLIC METHODS YOU CAN OVERWRITE BY YOUR OWN --- */
		// Event listeners
		on: {
			// Any type of AJAX request called by Shop module
			request: {
				start: 			function() { window.shop.loadingElement.show(); /* start loading */ },
				end: 			function() { window.shop.loadingElement.hide();  /* end loading */ }, 
				done: 			function(d) { 
					if (d && typeof d.cartItems == "number" && d.cartPrice) {
						$("[data-cart-insert='items']").text(d.cartItems);
						$("[data-cart-insert='price']").text(d.cartPrice);
					}
				},
				fail: 			function() { window.shop.alert("{{ trans("core::core.something went wrong") }}", "error") },
				validationDone: function() { $(".has-error").removeClass("has-error"); },
				validationFail: function(inputs) { 
					window.shop.alert("{{ trans("shop::cart.alerts.validation fail") }}", "error"); 
					$(".has-error").removeClass("has-error"); 
					inputs.parent().addClass("has-error");  	// add .has-error class to failed inputs' wrappers
				}
			},

			// Add item to cart request
			addRequest: {
				start: 	function() {  },
				end: 	function() {  },
				done: 	function() { window.shop.alert("{{ trans("shop::cart.alerts.add done") }}", "success") },
				fail: 	function() {  },
			},

			// Remove item from cart request
			removeRequest: {
				start: 	function() {  },
				end: 	function() {  },
				done: 	function(d) { 
					window.shop.alert("{{ trans("shop::cart.alerts.remove done") }}", "success");
					$("tr[data-row-id='"+d.rowId+"']").remove();
				},
				fail: 	function() { },
			},

			// Update item quantity request
			updateRequest: {
				start: 	function() {  },
				end: 	function() {  },
				done: 	function(d) { 
					window.shop.alert("{{ trans("shop::cart.alerts.update done") }}", "success");
					var row = $("tr[data-row-id='"+d.rowId+"']");

					if (d.quantity <= 0)
						row.remove();
					else {
						// Replace product price in table for new value
						var price = row.find("[data-price]").data("price");
						row.find(".total-price").html(window.shop.price(price*d.quantity));
						d.input.data("previous-value", d.quantity);
					}
				},
				fail: 	function(d) { 
					d.input.val(d.prevQuantity); // Revert input to its previous value
				},
			},

			// Destroy whole cart request
			destroyRequest: {
				start: 	function() {  },
				end: 	function() {  },
				done: 	function() { 
					window.shop.alert("{{ trans("shop::cart.alerts.destroy done") }}", "success");
					$(".cart-table tbody tr").remove();
				},
				fail: 	function() {  },
			},

			// Store customer data request
			customerDataRequest: {
				start: 			function() { },
				end: 			function() { },
				done: 			function() { /* customer data submitted - redirect to next step */ },
				fail: 			function() { },
				validationDone: function() { },
				validationFail: function() { }
			},

			// Store delivery data request
			deliveryDataRequest: {
				start: 			function() { },
				end: 			function() { },
				done: 			function() { /* delivery data submitted - redirect to next step */ },
				fail: 			function() { },
				validationDone: function() { },
				validationFail: function() { }
			},

			// Submit order request
			submitOrderRequest: {
				start: 	function() {  },
				end: 	function() {  },
				done: 	function() { /* order submitted - redirect to confirmation page */ },
				fail: 	function() {  },
			}
		},

		/**
		 * Alert about customer requests events
		 *
		 * @param string 	string 	text of alert
		 * @param string 	type 	type of alert (success or error)
		 */

		alert: function(string, type) {
			alert(string);
		},

		/** 
		 * Element to be hidden/shown on request end/start
		 *
		 * @var jQuery object
		 */
		loadingElement: $(".loading"), 	// feel free to overwrite


		/* --- PUBLIC METHODS TO BE USED --- */
		/**
		 * Format number to price based on Asgard settings
		 *
		 * @param number 	value 	number to be formatted
		 * @return string
		 */
		price: function(value) {
			var currency = "{{ config("asgard.shop.core.currency.string", " Kč") }}"
			var currencyBeforeNumber = {{ config("asgard.shop.core.currency.before number", false) ? "true" : "false" }};

			var decimals = "{{ config("asgard.shop.core.currency.decimals", "auto") }}";
			var decimalSeparator = "{{ config("asgard.shop.core.currency.decimalSeparator", ",") }}";
			var thousandSeparator = "{{ config("asgard.shop.core.currency.thousandSeparator", " ") }}";

			// Auto detect decimal number
			if (decimals == "auto") {
				if (Math.floor(value*100/100) == value) 	// number has no decimals
					decimals = 0;
				else 									// number has decimals
					decimals = 2;
			}

			return (currencyBeforeNumber ? currency : "")+window.shop._numberFormat(value, decimals, decimalSeparator, thousandSeparator)+(!currencyBeforeNumber ? currency : "");
		},

		/**
		 * Show/hide select values based on other select's values
		 * Automatically used for $("[data-order-delivery-field-name='country']"), $("[data-order-delivery-field-name='shipping']") 
		 * and $("[data-order-delivery-field-name='shipping']"), $("[data-order-delivery-field-name='payment']")
		 *
		 * @param jQuery object 	sourceSelect 		select whose value decides about target select's options
		 * @param jQuery object 	targetSelect 		select whose values are hidden/shown based on sourceSelect's value
		 * @param string 			sourceKeyAttribute	attribute of sourceSelect's options that contains targetSelect's key values to be shown separated by delimiter (e. g. "4,5,8") [default "data-filter-values"]
		 * @param string 			targetKeyAttribute 	attribute of targetSelect's options that contains value to be filtered by (e. g. "5") [default "value"]
		 * @param string 			delimited 			delimiter to split sourceKeyAttribute by in order to get array of values [default ","]
		 * @return void
		 */
		filterSelectValues: function(sourceSelect, targetSelect, sourceKeyAttribute, targetKeyAttribute, delimiter) {
			sourceKeyAttribute = sourceKeyAttribute || "data-filter-values";
			targetKeyAttribute = targetKeyAttribute || "value";
			delimiter  = delimiter || ",";
			sourceSelect.on("change", function() {
				var selectedOption = $(this).find("option:selected");
				var acceptValues = selectedOption.attr(sourceKeyAttribute).toString().split(delimiter);

				window.shop._filterSelectValues(targetSelect, acceptValues, targetKeyAttribute);
			});
			sourceSelect.change();
		},

		@include("shop::frontend.partials.private-functions")
	};

	{{-- Add product to cart --}}
	@if (!isset($omit) || !is_array($omit) || (!in_array("cart", $omit) && !in_array("cart-add", $omit)))
		@include("shop::frontend.partials.listeners.cart-add")
	@endif

	{{-- Change product quantity --}}
	@if (!isset($omit) || !is_array($omit) || (!in_array("cart", $omit) && !in_array("cart-update", $omit)))
		@include("shop::frontend.partials.listeners.cart-update")
	@endif

	{{-- Remove item from cart --}}
	@if (!isset($omit) || !is_array($omit) || (!in_array("cart", $omit) && !in_array("cart-remove", $omit)))
		@include("shop::frontend.partials.listeners.cart-remove")
	@endif

	{{-- Destroy whole cart --}}
	@if (!isset($omit) || !is_array($omit) || (!in_array("cart", $omit) && !in_array("cart-destroy", $omit)))
		@include("shop::frontend.partials.listeners.cart-destroy")
	@endif

	{{-- Submit customer data form --}}
	@if (!isset($omit) || !is_array($omit) || (!in_array("order", $omit) && !in_array("order-customer-data", $omit)))
		@include("shop::frontend.partials.listeners.order-customer-data")
	@endif

	{{-- Submit delivery data form --}}
	@if (!isset($omit) || !is_array($omit) || (!in_array("order", $omit) && !in_array("order-delivery-data", $omit)))
		@include("shop::frontend.partials.listeners.order-delivery-data")
	@endif

	{{-- Submit order --}}
	@if (!isset($omit) || !is_array($omit) || (!in_array("order", $omit) && !in_array("order-submit", $omit)))
		@include("shop::frontend.partials.listeners.order-submit")
	@endif
</script>