	// Submit customer data form
	$(document).on("click", "[data-submit-order-customer-data]", function() {
		var fields = $("[data-order-customer-field-name]");
		var data = window.shop._getFormData(fields, "order-customer-field-name", window.shop.on.customerDataRequest.validationFail, window.shop.on.customerDataRequest.validationDone);

		if (data) {
			window.shop.on.request.start();
			window.shop.on.customerDataRequest.start();
			$.ajax("{{ route("api.shop.order.store user data") }}", {
				data: {
					_token: "{{ csrf_token() }}",
					userData: data
				},
				type: "post"
			}).done(function(d) {
				if (d && d.ok) {
					window.shop.on.request.done(d);
					window.shop.on.customerDataRequest.done(d);
				} else {
					window.shop.on.request.fail(d);
					window.shop.on.customerDataRequest.fail(d);
				}
			}).fail(function(d) {
				window.shop.on.request.fail(d);
				window.shop.on.customerDataRequest.fail(d);
			}).always(function(d) {
				window.shop.on.request.end(d);
				window.shop.on.customerDataRequest.end(d);
			});
		}
	});