	// Remove item from cart
	$(document).on("click", "[data-remove-from-cart-id]", function() {
		var rowId = $(this).data("remove-from-cart-id");

		if (rowId) {
			window.shop.on.request.start();
			window.shop.on.removeRequest.start();
			$.ajax("{{ route("api.shop.cart.remove") }}", {
				data: {
					cartRowId: rowId,
					_token: "{{ csrf_token() }}"
				},
				type: "post"
			}).done(function(d) {
				d.rowId = rowId;
				if (d && d.ok) {
					window.shop.on.request.done(d);
					window.shop.on.removeRequest.done(d);
				} else {
					window.shop.on.request.fail(d);
					window.shop.on.removeRequest.fail(d);
				}
			}).fail(function(d) {
				d.rowId = rowId;
				window.shop.on.request.fail(d);
				window.shop.on.removeRequest.fail(d);
			}).always(function(d) {
				d.rowId = rowId;
				window.shop.on.request.end(d);
				window.shop.on.removeRequest.end(d);
			});
		}
	});