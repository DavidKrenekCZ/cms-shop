	// Destroy whole cart
	$(document).on("click", "[data-destroy-cart]", function() {
		window.shop.on.request.start();
		window.shop.on.destroyRequest.start();
		$.ajax("{{ route("api.shop.cart.destroy") }}", {
			data: {
				_token: "{{ csrf_token() }}"
			},
			type: "post"
		}).done(function(d) {
			if (d && d.ok) {
				window.shop.on.request.done(d);
				window.shop.on.destroyRequest.done(d);
			} else {
				window.shop.on.request.fail(d);
				window.shop.on.destroyRequest.fail(d);
			}
		}).fail(function(d) {
			window.shop.on.request.fail(d);
			window.shop.on.destroyRequest.fail(d);
		}).always(function(d) {
			window.shop.on.request.end(d);
			window.shop.on.destroyRequest.end(d);
		});
	});