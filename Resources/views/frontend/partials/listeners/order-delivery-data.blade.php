	// Submit delivery data form
	$(document).on("click", "[data-submit-order-delivery-data]", function() {
		var fields = $("[data-order-delivery-field-name]");
		var data = window.shop._getFormData(fields, "order-delivery-field-name", window.shop.on.deliveryDataRequest.validationFail, window.shop.on.deliveryDataRequest.validationDone);

		if (data) {
			window.shop.on.request.start();
			window.shop.on.deliveryDataRequest.start();
			$.ajax("{{ route("api.shop.order.store delivery data") }}", {
				data: {
					_token: "{{ csrf_token() }}",
					deliveryData: data
				},
				type: "post"
			}).done(function(d) {
				if (d && d.ok) {
					window.shop.on.request.done(d);
					window.shop.on.deliveryDataRequest.done(d);
				} else {
					window.shop.on.request.fail(d);
					window.shop.on.deliveryDataRequest.fail(d);
				}
			}).fail(function(d) {
				window.shop.on.request.fail(d);
				window.shop.on.deliveryDataRequest.fail(d);
			}).always(function(d) {
				window.shop.on.request.end(d);
				window.shop.on.deliveryDataRequest.end(d);
			});
		}
	});

	// Hide/show  shipping/payment method select values base on selected country/shipping method
	$(function() {
		if ($("[data-order-delivery-field-name='country']").length)
			window.shop.filterSelectValues($("[data-order-delivery-field-name='country']"), $("[data-order-delivery-field-name='shipping']"));

		if ($("[data-order-delivery-field-name='shipping']").length)
			window.shop.filterSelectValues($("[data-order-delivery-field-name='shipping']"), $("[data-order-delivery-field-name='payment']"));
	});