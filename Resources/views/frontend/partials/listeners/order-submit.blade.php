	// Submit order
	$(document).on("click", "[data-submit-order]", function() {
		window.shop.on.request.start();
		window.shop.on.submitOrderRequest.start();
		$.ajax("{{ route("api.shop.order.submit") }}", {
			data: {
				_token: "{{ csrf_token() }}"
			},
			type: "post"
		}).done(function(d) {
			if (d && d.ok) {
				window.shop.on.request.done(d);
				window.shop.on.submitOrderRequest.done(d);
			} else {
				window.shop.on.request.fail(d);
				window.shop.on.submitOrderRequest.fail(d);
			}
		}).fail(function(d) {
			window.shop.on.request.fail(d);
			window.shop.on.submitOrderRequest.fail(d);
		}).always(function(d) {
			window.shop.on.request.end(d);
			window.shop.on.submitOrderRequest.end(d);
		});
	});