	// Change product quantity
	$(document).on("change", "[data-change-quantity-id]", function(e) {
		var input = $(this);
		var rowId = input.data("change-quantity-id");
		var quantity = parseInt(input.val());
		var previousValue = input.data("previous-value");

		if (rowId && typeof quantity == "number" && !isNaN(quantity)) {
			window.shop.on.request.start();
			window.shop.on.updateRequest.start();
			$.ajax("{{ route("api.shop.cart.update") }}", {
				data: {
					cartRowId: rowId,
					quantity: quantity,
					_token: "{{ csrf_token() }}"
				},
				type: "post"
			}).done(function(d) {
				d.rowId = rowId;
				d.quantity = quantity;
				d.input = input;
				d.prevQuantity = previousValue;
				if (d && d.ok) {
					window.shop.on.request.done(d);
					window.shop.on.updateRequest.done(d);
				} else {
					window.shop.on.request.fail(d);
					window.shop.on.updateRequest.fail(d);
				}
			}).fail(function(d) {
				d.rowId = rowId;
				d.quantity = quantity;
				d.input = input;
				d.prevQuantity = previousValue;
				window.shop.on.request.fail(d);
				window.shop.on.updateRequest.fail(d);
			}).always(function(d) {
				d.rowId = rowId;
				d.quantity = quantity;
				d.input = input;
				d.prevQuantity = previousValue;
				window.shop.on.request.end(d);
				window.shop.on.updateRequest.end(d);
			});
		}
	});