	// Add product to cart
	$(document).on("click", "[data-add-to-cart-id]", function() {
		var productId = parseInt($(this).data("add-to-cart-id"));
		if (productId > 0) {
			window.shop.on.request.start();
			window.shop.on.addRequest.start();
			$.ajax("{{ route("api.shop.cart.insert") }}", {
				data: {
					id: productId,
					_token: "{{ csrf_token() }}"
				},
				type: "post"
			}).done(function(d) {
				if (d && d.ok) {
					window.shop.on.request.done(d);
					window.shop.on.addRequest.done(d);
				} else {
					window.shop.on.request.fail(d);
					window.shop.on.addRequest.fail(d);
				}
			}).fail(function(d) {
				window.shop.on.request.fail(d);
				window.shop.on.addRequest.fail(d);
			}).always(function(d) {
				window.shop.on.request.end(d);
				window.shop.on.addRequest.end(d);
			});
		}
	});