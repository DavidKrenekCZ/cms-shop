<!DOCTYPE html>
<html>
	<head>
		<title>DavidKrenekCZ / Shop module</title>
		@include("shop::frontend.includes.styles")
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
	</head>
	<body>
		@if (Route::current()->uri != "products")
			<nav class="back-to-products"><a href="{{ route("frontend.shop.products overview") }}">< Back to products</a></nav>
		@endif
		
		@include("shop::frontend.includes.cart-small")
		@include("shop::frontend.includes.custom-js")

		<div class="loading">
			Loading...
		</div>

		@yield("content")
	</body>
</html>