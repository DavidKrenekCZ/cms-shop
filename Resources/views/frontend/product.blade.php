@extends("shop::frontend.layout")

@section("content")
<div>
	<div class="product product-detail">
		<div>
			<a href="{{ $product->purl("products/") }}">{{ $product->name }}</a><br>
			<b>Code:</b> {{ $product->field("Code") }}<br>
			<b>Price:</b> {{ $product->field("Price") }}<br>
			<b>Description:</b> {!! $product->field("Description") !!}<br>
			<b>Note:</b> {!! $product->field("Note") !!}

			@if ($product->images->count())
			<div>
				<div class="row">
					<h4>{{ $product->images_title == "" ? "Gallery:" : $product->images_title }}</h4>
					@foreach ($product->images()->ordered() as $img)
					<div class="col-sm-6 col-md-4">
                        <a href="{{ $img->publicpath }}" target="_blank">
                            <img src="{{ $img->thumbnail2->publicpath }}" alt="" class="img-thumbnail">
                        </a>
                    </div>
                    @endforeach
				</div>
			</div>
			<br><br><br><br><br><br>
        	@endif

        	@if ($product->files->count())
			<div>
				<div class="row">
					<h4>{{ $product->files_title == "" ? "Uploaded files:" : $product->files_title }}</h4>
					@foreach ($product->files()->ordered() as $file)
					<li>
                        <a href="{{ $file->downloadpath }}">
                            <span>{{ $file->name }}</span>
                        </a>
                    </li>
                    @endforeach
				</div>
			</div>
			<br><br><br><br><br><br>
        	@endif

        	<a href="javascript:void(0)" data-add-to-cart-id="{{ $product->id }}">ADD TO CART</a>

		</div>
		@if ($product->mainImage)
		<img src="{{ $product->mainImage }}">
		@endif
	</div>
</div>
@endsection